import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdministrativosService {

  constructor(
    private http: HttpClient,
    public router: Router
  ) { }

  headers:HttpHeaders = new HttpHeaders({
    'Content-Type':'application/json'
  });

  prefijo_url_empleado = 'http://35.193.56.199:3005/';
  public token = '';
  public id_user = 2;

  // ESTADIOS -------------
  //Obtener estadios
  get_estadios(){
    const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_empleado + 'esb/stadium';
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Crear estadio
  new_estadio(name:string, foundation_date:string, capacity:number, address:string, photo:string,
    status:number, id_country:number){
    const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_empleado + 'esb/stadium';

    return this.http.post(url, {
      'name' : name,
      'foundation_date' : foundation_date,
      'capacity': capacity,
      'address' : address,
      'photo' : photo,
      'status' : status,    
      'id_country' : id_country
    }, {headers}
    ).pipe(map( data => data ));
  }

  //Modificar estadio
  update_estadio(name:string, foundation_date:string, capacity:number, address:string, photo:string,
    status:number, id_country:number, id:number){
    const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_empleado + 'esb/stadium';

    return this.http.put(url, {
      'name' : name,
      'foundation_date' : foundation_date,
      'capacity': capacity,
      'address' : address,
      'photo' : photo,
      'status' : status,    
      'id_country' : id_country,
      'id': id
    }, {headers}
    ).pipe(map( data => data ));
  }

  //Eliminar estadio
  delete_estadio(id:number){
    const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_empleado + 'esb/stadium';

    const options = {
      headers: headers,
      body: {
        id: id
      },
    };
    return this.http.delete(url, options
    ).pipe(map( data => data ));
  }

  //EQUIPO -----------------------
    //Obtener equipos
    get_equipos(){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/team';

      return this.http.get(url
          , { headers }).pipe(map( data => data ));
    }
  
    //Crear equipos
    new_equipo(name:string, foundation_date:string, photo:string, id_country:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/team';

      return this.http.post(url, {
        'name' : name,
        'foundation_date' : foundation_date,
        'photo' : photo,
        'id_country' : id_country
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Modificar equipos
    update_equipo(name:string, foundation_date:string, photo:string, id_country:number, id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/team';
  
      return this.http.put(url, {
        'name' : name,
        'foundation_date' : foundation_date,
        'photo' : photo,
        'id_country' : id_country,
        'id': id
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Eliminar equipo
    delete_equipo(id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/team';
      const options = {
      headers: headers,
      body: {
        id: id
      },
    };
    return this.http.delete(url, options
    ).pipe(map( data => data ));
    }

    //DIRECTOR TECNICO
    //obtener director tecnico
    get_dt(){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/technical-director';

      return this.http.get(url
          , { headers }).pipe(map( data => data ));
    }
  
    //Crear dt
    new_dt(name:string, lastname:string, birth_date:string, photo:string, status:number,
          id_country:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/technical-director';

      return this.http.post(url, {
        'name' : name,
        'lastname': lastname,
        'birth_date' : birth_date,
        'photo' : photo,
        'status': status,
        'id_country' : id_country
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Modificar dt
    update_dt(name:string, lastname:string, birth_date:string, photo:string, status:number, 
      id_country:number, id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/technical-director';
  
      return this.http.put(url, {
        'name' : name,
        'lastname': lastname,
        'birth_date' : birth_date,
        'photo' : photo,
        'status': status,
        'id_country': id_country,
        'id': id
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Eliminar dt
    delete_dt(id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/technical-director';
      const options = {
      headers: headers,
      body: {
        id: id
      },
    };
    return this.http.delete(url, options
    ).pipe(map( data => data ));
    }

    //JUGADOR
    //obtener jugador
    get_jugador(){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/player';

      return this.http.get(url
          , { headers }).pipe(map( data => data ));
    }
  
    //Crear jugador
    new_jugador(name:string, lastname:string, birth_date:string, id_nationality:number, position:number,
      photo:string, status:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/player';

      return this.http.post(url, {
        'name' : name,
        'lastname': lastname,
        'birth_date' : birth_date,
        'id_nationality': id_nationality,
        'position': position,
        'status': status,
        'photo' : photo
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Modificar jugador
    update_jugador(name:string, lastname:string, birth_date:string, id_nationality:number, position:number,
      photo:string, status:number, id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/player';
  
      return this.http.put(url, {
        'name' : name,
        'lastname': lastname,
        'birth_date' : birth_date,
        'id_nationality': id_nationality,
        'position': position,
        'status': status,
        'photo' : photo,
        'id': id
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Eliminar jugador
    delete_jugador(id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/player';
      const options = {
      headers: headers,
      body: {
        id: id
      },
    };
    return this.http.delete(url, options
    ).pipe(map( data => data ));
    }

    //COMPETENCIA
    //obtener competencia
    get_compe(){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/competition';

      return this.http.get(url
          , { headers }).pipe(map( data => data ));
    }
  
    //Crear competencia
    new_compe(name:string, type:number, year:number, id_country:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/competition';

      return this.http.post(url, {
        'name' : name,
        'type': type,
        'year' : year,
        'id_country': id_country
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Modificar competencia
    update_compe(name:string, type:number, year:number, id_country:number, id:number, id_champion_team:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/competition';
  
      return this.http.put(url, {
        'name' : name,
        'type': type,
        'year' : year,
        'id_champion_team': id_champion_team,
        'id_country': id_country,
        'id': id
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Eliminar competicion
    delete_compe(id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/competition';
      const options = {
      headers: headers,
      body: {
        id: id
      },
    };
    return this.http.delete(url, options
    ).pipe(map( data => data ));
    }

    //PARTIDO
    //Crear partido
    get_partido(){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/match';

      return this.http.get(url
          , { headers }).pipe(map( data => data ));
    }
  
    //Crear partido
    new_partido(game_date:string, attendees:number, result_local:number, result_visiting:number,
      status:number, id_stadium:number, id_team_local:number, id_team_visiting:number, id_competition:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/match';

      return this.http.post(url, {
        'game_date': game_date,
        'attendees': attendees,
        'result_local': result_local,
        'result_visiting': result_visiting,
        'status': status,
        'id_stadium' : id_stadium,
        'id_team_local' : id_team_local,
        'id_team_visiting' : id_team_visiting,
        'id_competition': id_competition
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Modificar partido
    update_partido(game_date:string, attendees:number, result_local:number, result_visiting:number,
      status:number, id_stadium:number, id_team_local:number, id_team_visiting:number, id_competition:number, 
      id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/match';
  
      return this.http.put(url, {
        'game_date': game_date,
        'attendees': attendees,
        'result_local': result_local,
        'result_visiting': result_visiting,
        'status': status,
        'id_stadium' : id_stadium,
        'id_team_local' : id_team_local,
        'id_team_visiting' : id_team_visiting,
        'id_competition': id_competition,
        'id': id
      }, {headers}
      ).pipe(map( data => data ));
    }
  
    //Eliminar partido
    delete_partido(id:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/match';
      const options = {
      headers: headers,
      body: {
        id: id
      },
    };
    return this.http.delete(url, options
    ).pipe(map( data => data ));
    }

    //ACCIONES EMPLEADO
    //Transferir jugador
    new_transferir_j(id_player:number, id_team_origin:number, id_team_destination:number, start_date:string, 
      end_date:string){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/employee/player-transfer';

      return this.http.post(url, {
        "id_player": id_player,
        "id_team_origin" : id_team_origin,
        "id_team_destination" : id_team_destination,
        "start_date": start_date,
        "end_date": end_date
      }, {headers}
      ).pipe(map( data => data ));
    }

    //Transferir director tecnico
    new_transferir_dt(id_coach:number, id_team_origin:number, id_team_destination:number, start_date:string, 
      end_date:string){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/employee/technical-director-transfer';

      return this.http.post(url, {
        "id_coach": id_coach,
        "id_team_origin" : id_team_origin,
        "id_team_destination" : id_team_destination,
        "start_date": start_date,
        "end_date": end_date
      }, {headers}
      ).pipe(map( data => data ));
    }

    //Agregar incidencia 
    new_incidencia(id_player:number, id_game:number, id_type:number, description:string, 
      minute:string){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/employee/incidence';

      return this.http.post(url, {
        "id_player": id_player,
        "id_game" : id_game,
        "id_type" : id_type,
        "description": description,
        "minute": minute
      }, {headers}
      ).pipe(map( data => data ));
    }

    //Agregar Post 
    new_post(title:string, description:string, date:string, id_team:number, id_user:number){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url_empleado + 'esb/post';

      return this.http.post(url, {
        "title": title,
        "description" : description,
        "date" : date,
        "id_team": id_team,
        "id_user": id_user
      }, {headers}
      ).pipe(map( data => data ));
    }
}
