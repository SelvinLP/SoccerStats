import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient,
    public router: Router
  ) { }

  public rol = 'Noecontrado';
  prefijo_url = 'http://35.193.56.199:3005/';
  prefijo_url_admin = 'http://35.193.56.199:3005/';
  prefijo_url_autenticacion = 'http://35.193.56.199:3005/';
  public token = '';

  headers:HttpHeaders = new HttpHeaders({
    'Content-Type':'application/json'
  });
  public data;
  public id;

  headerstoken_get:HttpHeaders = new HttpHeaders();

   // Registrar Usuario
   registrar_usu(name:string, lastname:string, password:string, email:string, phone:number,
    photo:string, gender:string, birth_date:string, address:string, id_country:number){

    const url = this.prefijo_url + 'esb/customer/register';

    return this.http.post(url, {
      'name' : name,
      'lastname' : lastname,
      'password': password,
      'email' : email,
      'phone' : phone,
      'photo' : photo,    
      'gender' : gender,
      'birth_date' : birth_date,
      'address' : address,
      'id_country' : id_country
  
    }, {headers: this.headers}
    ).pipe(map( data => data ));
  }


   //Envio de contraseña temporal
   Temp_pass(email:string){
    const url = this.prefijo_url_autenticacion + 'esb/auth/reset-password';
      return this.http.post(url, {
        'email': email
      }, {headers: this.headers}
    ).pipe(map( data => data ));
  }

  //Restablecer contraseña
  Rec_pass(email:string, temporal_password:string,new_password:string){
    const url = this.prefijo_url_autenticacion + 'esb/auth/reset-password';
      return this.http.post(url, {
        'email': email,
        'temporal_password': temporal_password,
        'new_password': new_password
      }, {headers: this.headers}
    ).pipe(map( data => data ));
  }

  //Login
  login(email:string, password :string){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url + 'esb/auth';
      return this.http.post(url, {
        'email': email,
        'password': password
      }, {headers: this.headers}
    ).pipe(map( data => data ));
  }

   //Visualizar Perfil
   perfil(){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url + 'esb/customer/?id=' + this.id;
      return this.http.get(url,{ headers }).pipe(map( data => data ));
  }

  //Modificar Perfil
  modificar_usuario(id:number, name:string, lastname:string, password:string, email:string, phone:string,
    photo:string, gender:string, birth_date:string, address:string, id_country:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*', 'Content-Type':'application/json'}
    const url = this.prefijo_url + 'esb/customer/';
      return this.http.put(url, {
        'id': id,
        'name': name,
        'lastname': lastname,
        'password': password,
        'email': email,
        'phone': phone,
        'photo': photo,
        'gender': gender,
        'birth_date': birth_date,
        'address': address,
        'id_country': id_country
      }, {headers: headers}
    ).pipe(map( data => data ));
  }

  eliminar_user(id:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*', 'Content-Type':'application/json'}
    const url = this.prefijo_url + 'esb/customer/';
      return this.http.put(url, {
        'id': id
      }, {headers: headers}
    ).pipe(map( data => data ));
  }

  //Reporte 1
  reporte1(idteam:number, idplayer:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url + 'esb/customer/report/1?player='+idplayer+'&id_team='+idteam;
      return this.http.get(url,{ headers }).pipe(map( data => data ));
  } 

  //Reporte 2: 'Jugadores o Técnico mayores a X años'
  reporte2(age:number, player:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url + 'esb/customer/report/2?age=' + age+'&player='+player;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 3: 'Jugadores o Técnico menores a X años'
  reporte3(age:number, player:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url + 'esb/customer/report/3?age=' + age+'&player='+player;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 4: 'Equipos con X años de antigüedad'
  reporte4(age:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url + 'esb/customer/report/6?age=' + age;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

    //Reporte 5: 'Estadios con capacidad menor o igual a X'
    reporte5(capacity:number){
      const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url + 'esb/customer/report/8?capacity=' + capacity;
        return this.http.get(url
          , { headers }).pipe(map( data => data ));
    }

    //Reporte 6: 'Partidos donde hubo al menos X goles'
    reporte6(goals:number){
      const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url + 'esb/customer/report/11?goals=' + goals;
        return this.http.get(url
          , { headers }).pipe(map( data => data ));
    }


  //ADMINISTRADOR ----------------------------------------------------------------------
  //Creacion de usuario
  new_user(name:string, lastname:string, password:string, email:string, phone:number, photo:string,
    gender:string, birth_date:string, address:string, id_rol:number, id_country:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*', 'Content-Type':'application/json'}
    const url = this.prefijo_url_admin + 'esb/administrator/user';
      return this.http.post(url, {
        'name': name,
        'lastname': lastname,
        'password': password,
        'email': email,
        'phone': phone,
        'photo': photo,
        'gender': gender,
        'birth_date': birth_date,
        'address': address,
        'id_rol': id_rol,
        'id_country': id_country
      }, {headers: headers}
    ).pipe(map( data => data ));
  }
  
  update_user(id:number, name:string, lastname:string, password:string, email:string, phone:number, photo:string,
    gender:string, birth_date:string, address:string, id_country:number, description:string){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*', 'Content-Type':'application/json'}
    const url = this.prefijo_url_admin + 'esb/administrator/user';
      return this.http.put(url, {
        'id': id,
        'name': name,
        'lastname': lastname,
        'password': password,
        'email': email,
        'phone': phone,
        'photo': photo,
        'gender': gender,
        'birth_date': birth_date,
        'address': address,
        'id_country': id_country,
        'description': description
      }, {headers: headers}
    ).pipe(map( data => data ));
  }

  //Congelar Usuario
  update_status_user(id:number, id_status:number, description:string){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*', 'Content-Type':'application/json'}
    const url = this.prefijo_url_admin + 'esb/administrator/user/status';
      return this.http.put(url, {
        'id': id,
        'id_status': id_status,
        'description': description
      }, {headers: headers}
    ).pipe(map( data => data ));
  }

  //Obtener usuarios
  get_users(){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/user';
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Obtener bitacora
  get_bitacora(){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/10';
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 1 - Usuarios Suscritos a X equipo
  get_reporte1(equipo:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/1?id_team=' + equipo;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 2 - Usuarios con o sin membresia 
  get_reporte2(membership:boolean){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/2?membership=' + membership;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 3 - Usuarios que Mas membresías han adquirido
  get_reporte3(){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/3';
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 4 - Usuarios que más dinero han gastado
  get_reporte4(){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/4';
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 5 - Usuarios de x pais
  get_reporte5(id_country:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/5?id_country=' + id_country;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 6 - Usuarios de x genero
  get_reporte6(gender:string){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/6?gender=' + gender;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

  //Reporte 7 - Usuarios con al menos x años de edad
  get_reporte7(age:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/7?age=' + age;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }
  
 //Reporte 8 - Empleados que MAS/MENOS noticias han publicado
  get_reporte8(order:number){
    const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url_admin + 'esb/administrator/report/8?order=' + order;
      return this.http.get(url
        , { headers }).pipe(map( data => data ));
  }

 //Reporte 9 - Empleados que MAS/MENOS noticias han publicado de X Equipo
 get_reporte9(order:number, id_team:number){
  const headers = { 'authorization': "Bearer" +this.token, 'Access-Control-Allow-Origin':'*'}
  const url = this.prefijo_url_admin + 'esb/administrator/report/9?order=' + order + '&id_team=' + id_team;
    return this.http.get(url
      , { headers }).pipe(map( data => data ));
  }

}
