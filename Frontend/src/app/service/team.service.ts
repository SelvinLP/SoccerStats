
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(
    private http: HttpClient,
    public router: Router
  ) { }

  prefijo_url = 'http://35.223.13.4:5001/';
  public token = '';

  headers:HttpHeaders = new HttpHeaders({
    'Content-Type':'application/json'
  });
  public data;
  public id;

  headerstoken_get:HttpHeaders = new HttpHeaders();

    //Obtener todos los equipos
  getTeams(){
    const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
    const url = this.prefijo_url + 'esb/team';
      return this.http.get(url,{ headers }).pipe(map( data => data ));
  }

    //Obtener un equipo especifico
    getTeam(idteam:string){
      const headers = { 'x-access-tokens': this.token, 'Access-Control-Allow-Origin':'*'}
      const url = this.prefijo_url + 'esb/team?id=' + idteam;
        return this.http.get(url,{ headers }).pipe(map( data => data ));
    }

}