import { Component, OnInit } from '@angular/core';
import { CountryService } from 'src/app/service/countryservice';
import { AdministrativosService } from 'src/app/service/administrativos.service';

@Component({
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    constructor(
        public country_service: CountryService,
        public empleado_service: AdministrativosService
        ) {}

    ngOnInit() {
        this.get_equipo();
        this.get_dt();
        this.get_j();
        this.get_p();
    }

    seleccionado = {
        "id_player": { code: 1, name:'Selvin'},
        "id_team_origin" : { code: 0, name:'Barcelona'},
        "id_team_destination" :  { code: 0, name:'Real Madrid'},
        "start_date": "1999-01-01",
        "end_date": "1999-01-02"
      }

    seleccionado_dt = {
        "id_coach": { code: 1, name:'Selvin'},
        "id_team_origin" : { code: 0, name:'Barcelona'},
        "id_team_destination" :  { code: 0, name:'Real Madrid'},
        "start_date": "1999-01-01",
        "end_date": "1999-01-02"
    }

    data_dt = [
        { code: 3, name:'Selvin'}
    ]

    data_j = [
        { code: 4, name:'Lisandro'}
    ]

    data_equipo = [
        { code: 4, name:'Barcelona'}
    ]

    get_equipo(){
        this.data_equipo = [];
        this.empleado_service.get_equipos().subscribe((res: any) => {
            res.data.forEach((element) => {
              this.data_equipo.push({code: element.id, name: element.name});
            });
          });
    }

    get_dt(){
        this.data_dt = [];
        this.empleado_service.get_dt().subscribe((res: any) => {
            res.data.forEach((element) => {
              this.data_dt.push({code: element.id, name: element.name});
            });
          });
    }

    get_j(){
        this.data_j = [];
        this.empleado_service.get_jugador().subscribe((res: any) => {
            res.data.forEach((element) => {
              this.data_j.push({code: element.id, name: element.name});
            });
          });
    }

    tranferir_j(){
        this.empleado_service.new_transferir_j(
            this.seleccionado.id_player.code,
            this.seleccionado.id_team_origin.code,
            this.seleccionado.id_team_destination.code,
            this.seleccionado.start_date,
            this.seleccionado.end_date
          ).subscribe((res: any) => {
            console.log(res);
            alert('Jugador tranferido correctamente');
          }); 
    }

    tranferir_dt(){
        this.empleado_service.new_transferir_dt(
            this.seleccionado_dt.id_coach.code,
            this.seleccionado_dt.id_team_origin.code,
            this.seleccionado_dt.id_team_destination.code,
            this.seleccionado_dt.start_date,
            this.seleccionado_dt.end_date
          ).subscribe((res: any) => {
            console.log(res);
            alert('Director tecnico tranferido correctamente');
          }); 
    }


    //INCIDENCIA
    seleccionado_i = {
        "id_player": { code: 1, name:'Selvin'},
        "id_game" : { code: 1, name:'Clasico'},
        "id_type" : { code: 1, name:'Gol'},
        "description": "",
        "minute": ""
      }

    tipo_i = [
        { code: 1, name:'Gol'},
        { code: 2, name:'Autogol'},
        { code: 3, name:'Tarjeta Amarilla'},
        { code: 4, name:'Tarjeta Roja'}
    ]

    data_p = [{ code: 1, name:'Gol'}]

    get_p(){
        this.data_p = [];
        this.empleado_service.get_partido().subscribe((res: any) => {
            res.data.forEach((element) => {
              this.data_p.push({code: element.id, name: element.team_local + ' | ' + element.team_visiting});
            });
          });
    }

    new_incidencia(){
        this.empleado_service.new_incidencia(
            this.seleccionado_i.id_player.code,
            this.seleccionado_i.id_game.code,
            this.seleccionado_i.id_type.code,
            this.seleccionado_i.description,
            this.seleccionado_i.minute
          ).subscribe((res: any) => {
            console.log(res);
            alert('Incidencia agragada correctamente');
          }); 
    }

    //NOTICIA
    sle_noticia= {
        "id_team": { code: 1, name:'Gol'},
        "id_user": 1,
        "title": "",
        "description": "",
        "date": ""
    }

    new_noticia(){
        this.sle_noticia.id_user = this.empleado_service.id_user;
        this.empleado_service.new_post(
            this.sle_noticia.title,
            this.sle_noticia.description,
            this.sle_noticia.date,
            this.sle_noticia.id_team.code,
            this.sle_noticia.id_user
          ).subscribe((res: any) => {
            console.log(res);
            alert('Noticia agragada correctamente');
          }); 
    }
}
