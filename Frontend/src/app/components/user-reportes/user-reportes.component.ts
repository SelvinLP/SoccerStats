import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/service/usuario.service';
import { TeamService } from 'src/app/service/team.service';
import jsPDF from 'jspdf';
import { AdministrativosService } from 'src/app/service/administrativos.service';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-user-reportes',
  templateUrl: './user-reportes.component.html',
  styleUrls: ['./user-reportes.component.scss']
})
export class UserReportesComponent implements OnInit {

  constructor(public user_service: UsuarioService, public team_service: TeamService, public empleado_service: AdministrativosService) { }

  ngOnInit(): void {
    this.ObtenerEquipos();
  }

  //false = jugadores
  //true = DT
  DTjugadores= false;
  Equipos = [];
  Seleccionado5 = 1;

  data_reportes = [
    { code: 'r1', name:'Jugadores o Técnico de X equipo'},
    { code: 'r2', name:'Jugadores o Técnico mayores a X años'},
    { code: 'r3', name:'Jugadores o Técnico menores a X años'},
    { code: 'r4', name:'Equipos con X años de antigüedad'},
    { code: 'r5', name:'Estadios con capacidad menor o igual a X'},
    { code: 'r6', name:'Partidos donde hubo al menos X goles'}
  ];
  sle_rp_pdf = { code: 'r1', name:'Jugadores o Técnico de X equipo'};

  JugadoresDT = [
    {code:'1', name:'Jugadores'},
    {code:'2', name:'Directores Técnicos'}
  ];

  descargarpdf(){
    var datapdf = document.getElementById(this.sle_rp_pdf.code);
    html2canvas(datapdf).then((canvas) => {
      var imgWidth = 208;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4');
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('G_E.pdf');
    })
  }
 

  EquipoId=' ';
  unavez=0;

  GetEquipos(){
    if(this.unavez==0){
      this.ObtenerEquipos();
      this.unavez=1;
      return this.Equipos;
    }
    else{
      return this.Equipos;
    }
  }
 

   ObtenerEquipos(){
    this.Equipos = [];
    this.empleado_service.get_equipos().subscribe((res: any) => {
        res.data.forEach((element) => {
          this.Equipos.push({code: element.id, name: element.name});
        });
      });
  }

  data_r1 = [{
    'id': 1,
    'name': '',
    'lastname': '',
    'nationality': '',
    'position': ''
  }];

  data_r2 = [{
    'id': 1,
    'name': '',
    'lastname': '',
    'nationality': '',
    'photo': '',
    'position': '',
    'team': '',
    'age': 1
  }];
  data_r3 = [{
    'id': 1,
    'name': '',
    'lastname': '',
    'nationality': '',
    'photo': '',
    'position': '',
    'team': '',
    'age': 1
  }];

  data_r4 = [{
    'id_team': 1,
    'team': '',
    'photo': '',
    'foundation_date': '',
    'country': 2
  }];

  data_r5 = [{
    'id_stadium': 1,
    'stadium': '',
    'photo': '',
    'country': 1,
    'capacity': 10
  }];

  data_r6 = [{
    'id': 1,
    'game_date': '',
    'attendees': 1,
    'result_local': 1,
    'result_visiting': 1,
    'status': 1,
    'id_stadium': 1,
    'stadium': '',
    'id_team_local': 1,
    'team_local': '',
    'photo_local': '',
    'id_team_visiting': 1,
    'team_visiting': '',
    'photo_visiting': '',
    'id_competition': 1,
    'competition': ''
  }];

  Seleccionado1 = { code: 1, name:'Barcelona'};
  Seleccionado2 = { code: 2, name:'Jugadores'};


  get_Reporte1(){
    this.user_service.reporte1(this.Seleccionado1.code,this.Seleccionado2.code).subscribe((res: any) => {
      this.data_r1 = [];
      if(res.data){
      res.data.forEach((element) => {
        this.data_r1.push(element);
      });
    }
    });
    this.unavez=0;
  }

  player =0;
  Seleccionado3 = 20;
  Seleccionado4 = 20;
  Seleccionado6 = 20;
  Seleccionado7 = 2;

  set_jugador(){
    this.player=0;
  }
  set_tecnico(){
    this.player=1;
  }
  get_Reporte2(){
    this.data_r2 = [];
    this.user_service.reporte2(this.Seleccionado3,this.player).subscribe((res: any) => {
      if(res.data){
      res.data.forEach((element) => {
        this.data_r2.push(element);
      });}
    });
  }
  get_Reporte3(){
    this.data_r3 = [];
    this.user_service.reporte3(this.Seleccionado4,this.player).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r3.push(element);
      });
    });
  }

  get_Reporte4(){
    this.data_r4 = [];
    this.user_service.reporte4(this.Seleccionado5).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r4.push(element);
      });
    });
  }

  get_Reporte5(){
    this.data_r5 = [];
    this.user_service.reporte5(this.Seleccionado6).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r5.push(element);
      });
    });
  }
  get_Reporte6(){
    this.data_r6 = [];
    this.user_service.reporte6(this.Seleccionado7).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r6.push(element);
      });
    });
  }

}
