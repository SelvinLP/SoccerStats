import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserReportesComponent } from './user-reportes.component';

describe('UserReportesComponent', () => {
  let component: UserReportesComponent;
  let fixture: ComponentFixture<UserReportesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserReportesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserReportesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
