import { Component, OnInit } from '@angular/core';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-admin-bitacora',
  templateUrl: './admin-bitacora.component.html',
  styleUrls: ['./admin-bitacora.component.scss']
})
export class AdminBitacoraComponent implements OnInit {

  constructor(
    public user_service: UsuarioService
  ) { }

  ngOnInit(): void {
    this.get_bitacora();
  }

  data = [{
    'id'            : 3,
    'user_name'     : '',
    'user_lastname' : '',
    'user_photo'    : '',
    'user_role'     : 1,
    'action'        : '',
    'date'          : '2022-01-01',
    'description'   : '',
    'admin_id'      : 1,
  }];

  get_bitacora(){
    this.data = [];
    this.user_service.get_bitacora().subscribe((res: any) => {
      res.data.forEach((element) => {
        if(element.user_role == 1){
          element.user_role = { code: 1, name:'Administrador'};
        }else if(element.user_role == 2){
          element.user_role = { code: 2, name:'Empleado'};
        }else{
          element.user_role = { code: 3, name:'Cliente'};
        }
        this.data.push(element);
      });
    });
  }

  descargarpdf(){
    var datapdf = document.getElementById('r1');
    html2canvas(datapdf).then((canvas) => {
      var imgWidth = 208;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4');
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('G_E.pdf');
    })
  }
}
