import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfigService } from '../../service/app.config.service';
import { AppConfig } from '../../api/appconfig';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/service/usuario.service';
import { TeamService } from 'src/app/service/team.service';
import { AdministrativosService } from 'src/app/service/administrativos.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles:[`
    :host ::ng-deep .p-password input {
    width: 100%;
    padding:1rem;
    }

    :host ::ng-deep .pi-eye{
      transform:scale(1.6);
      margin-right: 1rem;
      color: var(--primary-color) !important;
    }

    :host ::ng-deep .pi-eye-slash{
      transform:scale(1.6);
      margin-right: 1rem;
      color: var(--primary-color) !important;
    }
  `]
})
export class LoginComponent implements OnInit, OnDestroy {

  valCheck: string[] = ['remember'];

  password: string;
  email: string;
  
  config: AppConfig;
  
  subscription: Subscription;

  constructor(
    public configService: ConfigService,
    public router: Router,
    public user_service: UsuarioService,
    public team_service: TeamService,
    public empleado_service: AdministrativosService
    ){ }

  ngOnInit(): void {
    this.config = this.configService.config;
    this.subscription = this.configService.configUpdate$.subscribe(config => {
      this.config = config;
    });
  }

  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe();
    }
  }

  login(){
    if(
      this.password != '' &&
      this.email != ''
    ){
      this.user_service.login(
        this.email,
        this.password
      ).subscribe((res: any) => {
        try {
          if(res.data.id_rol == 3 && res.data.user_validated && res.status == 200){
            this.user_service.id = res.data.id_user;
            this.user_service.rol = 'Cliente';
            this.user_service.token = res.data.token;
            this.team_service.token = res.data.token;
            this.empleado_service.token = res.data.token;
            this.empleado_service.id_user = res.data.id_user;
            if(res.data.password_type == 0){
              this.router.navigate(['/pages/inicio']);
            }else{
              this.router.navigate(['/pages/profile']);
              alert('Debe cambiar de contraseña');
            }
          }else if(res.status == 200 && res.data.id_rol != 3 && res.data.id_status == 1){
            this.user_service.id = res.data.id_user;
            if(res.data.id_rol == 1){
              this.user_service.rol = 'Administrador';
            }else if(res.data.id_rol == 2){
              this.user_service.rol = 'Empleado';
            }
            this.user_service.token = res.data.token;
            this.team_service.token = res.data.token;
            this.empleado_service.token = res.data.token;
            this.empleado_service.id_user = res.data.id_user;
            this.router.navigate(['/pages/inicio']);
          }else if(!res.data.user_validated){
            alert("El usuario no ha validado su correo")
          }else if(res.data.id_status != 1){
            alert("El usuario no esta habilitado")
          }else{
            alert(res.msj);
          }
        } catch (error) {
          alert(res.msj);
        }
        
        
      });
    }
    
  }

  profile(){
    this.router.navigate(['/pages/profile']);
  }

  registro(){
    this.router.navigate(['/pages/signup']);
  }

  rec_pass(){
    if(this.email == ''){
      alert('El correo no ha sido ingresado');
    }else{
      this.user_service.Temp_pass(
        this.email
      ).subscribe((res: any) => {
        alert('Correo de recuperación enviado');
      });
    }
  }

}
