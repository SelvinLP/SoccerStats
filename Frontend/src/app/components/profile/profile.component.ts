import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ConfigService } from '../../service/app.config.service';
import { AppConfig } from '../../api/appconfig';
import { Subscription } from 'rxjs';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

 

  config: AppConfig;
  
  subscription: Subscription;

  constructor(public configService: ConfigService, public user_service: UsuarioService,private sanitizer: DomSanitizer) { }

  // CAMPOS PARA NUEVA IMAGEN
  imagen_user:string = "";
  img_nombre:string = "";
  img_extension:string = "";
  img_base64:any = "";

  ngOnInit(): void {
    this.config = this.configService.config;
    this.subscription = this.configService.configUpdate$.subscribe(config => {
      this.config = config;
    });
    this.perfil();
    console.log("Perfil cargado");
  }

  onUpload(event:any) {
    if(event.target.files) {

      // GET NAME IMAGE
      this.img_nombre = event.target.files[0].name?.toString();
      this.img_nombre = this.img_nombre.replace(/\.[^/.]+$/, "")

      // GET EXTENSION
      this.img_extension = event.target.files[0].name?.toString();
      this.img_extension = this.img_extension.slice((this.img_extension.lastIndexOf(".") - 1 >>> 0) + 2);

      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = ( event2:any ) => {
        this.imagen_user = event2.target.result;
        this.img_base64 = reader.result?.toString();
        this.img_base64 = this.img_base64.replace("data:image/jpeg;base64,", "").replace("data:image/png;base64,", "").replace("data:image/png;base64,", "").replace("data:image/bmp;base64,", "").replace("data:image/raw;base64,", "").replace("data:image/tiff;base64,", "");
      }
    }
  }

  usuario = {
    id : 1,
    name: "",
    lastname: "",
    password: "",
    email: "",
    phone: 0,
    photo: "",
    gender: "M",
    birth_date: "",
    address: "",
    age: 20,
    country: ""
  };

  seleccionado = {
    id : 1,
    name: "",
    lastname: "",
    email: "",
    phone: "",
    photo: "",
    gender: "M",
    birth_date: "",
    address: "",
    password: "",
    age: "",
    idcountry: 1,
    country: ""
  };

  perfil(){
    this.user_service.perfil().subscribe((res: any) => {
      console.log(res);
        this.user_service.data = res.data;
        // this.usuario.id = res.data.user_id;  
        this.usuario.name = res.data.name; 
        this.usuario.lastname = res.data.lastname;
        this.usuario.age = res.data.age; 
        this.usuario.email = res.data.email;   
        this.usuario.phone = res.data.phone;  
        this.usuario.gender = res.data.gender;
        this.usuario.birth_date = res.data.birth_date;    
        this.usuario.photo = "data:image/jpeg;base64,"+res.data.photo;
        this.seleccionado.photo = res.data.photo;
        this.usuario.address = res.data.address;
        this.usuario.country = res.data.country;   
      
    });

  }

  


  update_user(){
    if(this.seleccionado.password != "")
    {
      alert('Ingresar contraseña anterior para actualizarla');
    }
    this.user_service.modificar_usuario(
      this.usuario.id,
      this.seleccionado.name,
      this.seleccionado.lastname,
      this.seleccionado.password,
      this.seleccionado.email,
      this.seleccionado.phone,
      this.seleccionado.photo,
      this.seleccionado.gender,
      this.seleccionado.birth_date,
      this.seleccionado.address,
      this.seleccionado.idcountry
    ).subscribe((res: any) => {
      if(res.status==200){
        
      alert('Usuario modificado con exito');
      }
      else {
        
      alert(res.message);
      }
    });
  }


  eliminar_user(){
    this.user_service.eliminar_user(
      this.usuario.id
    ).subscribe((res: any) => {
      if(res.status==200){
        
      alert('Cuenta eliminada con exito');
      }
      else {
        
      alert(res.message);
      }
    });
  }
}
