import { Component, OnInit } from '@angular/core';
import { AdministrativosService } from 'src/app/service/administrativos.service';
import { CountryService } from 'src/app/service/countryservice';

@Component({
    templateUrl: './misc.component.html',
    styleUrls: ['./misc.component.scss']
})
export class MiscComponent implements OnInit {

    constructor(
        public country_service: CountryService,
        public empleado_service: AdministrativosService
    ){
        
    }

    ngOnInit() {
        this.get_stadios();
        this.get_equipo();
        this.get_dt();
        this.get_j();
        this.get_c();
        this.get_p();
    }

    new_estadio = {
        id: 0,
        name: "",
        foundation_date: "1999-01-01",
        capacity: 10,
        address: '',
        photo: "",
        status: { code: 1, name:'Disponible'},
        id_country: { code: 88, name:'Guatemala'}
    };
    sl_estadio = this.country_service.dropdownItems;
    sl_estado = [
        { code: 1, name:'Disponible'},
        { code: 2, name:'Remodelación'}
    ]
    data_estadio = [{
        "id"                : 0,
        "name"              : '',
        "foundation_date"   : '',
        "capacity"          : '',
        "id_country"        : 88,
        "country"           : 'Guatemala',
        "address"           : '',
        "status"            : '',
        "photo"             : ''

    }]

    get_stadios(){
        this.data_estadio = [];
        this.empleado_service.get_estadios().subscribe((res: any) => {
            res.data.forEach((element) => {
              this.data_estadio.push(element);
            });
          });
    }
    
    seleccionar_estadio(elemento){
        this.new_estadio = {
            id: elemento.id,
            name: elemento.name,
            foundation_date: elemento.foundation_date,
            capacity: elemento.capacity,
            address: elemento.address,
            photo: elemento.photo,
            status: { code: elemento.status, name: elemento.status == 1 ? 'Disponible': 'Remodelación'},
            id_country: { code: elemento.id_country, name:elemento.country}
        }
    }

    new_estadiobtn(){
      this.new_estadio.photo = this.img_base64;
        this.empleado_service.new_estadio(
            this.new_estadio.name,
            this.new_estadio.foundation_date,
            this.new_estadio.capacity,
            this.new_estadio.address,
            this.new_estadio.photo,
            this.new_estadio.status.code,
            this.new_estadio.id_country.code
          ).subscribe((res: any) => {
            console.log(res);
            alert('Estadio creado correctamente');
          }); 
    }

    update_estadio(){
      this.new_estadio.photo = this.img_base64;
        this.empleado_service.update_estadio(
            this.new_estadio.name,
            this.new_estadio.foundation_date,
            this.new_estadio.capacity,
            this.new_estadio.address,
            this.new_estadio.photo,
            this.new_estadio.status.code,
            this.new_estadio.id_country.code,
            this.new_estadio.id
          ).subscribe((res: any) => {
            console.log(res);
            alert('Estadio modificado correctamente');
          });
    }

    delete_estadio(){
        this.empleado_service.delete_estadio(
            this.new_estadio.id
          ).subscribe((res: any) => {
            console.log(res);
            alert('Estadio eliminado correctamente');
          });
    }

    // EQUIPO
    sle_equipo = {
        id: 0,
        name: "",
        foundation_date: "1999-01-01",
        photo: "",
        id_country: { code: 88, name:'Guatemala'}
    };
    sl_equipo = this.country_service.dropdownItems;
    data_equipo = [{
        "id"                : 0,
        "name"              : '',
        "foundation_date"   : '',
        "id_country"        : 88,
        "country"           : 'Guatemala',
        "photo"             : ''
    }]

    get_equipo(){
        this.data_equipo = [];
        this.empleado_service.get_equipos().subscribe((res: any) => {
            res.data.forEach((element) => {
              this.data_equipo.push(element);
            });
          });
    }

    seleccionar_equipo(elemento){
        this.sle_equipo = {
            id: elemento.id,
            name: elemento.name,
            foundation_date: elemento.foundation_date,
            photo: elemento.photo,
            id_country: { code: elemento.id_country, name:elemento.country}
        }
    }

    new_equipo(){
      this.sle_equipo.photo = this.img_base64;
        this.empleado_service.new_equipo(
            this.sle_equipo.name,
            this.sle_equipo.foundation_date,
            this.sle_equipo.photo,
            this.sle_equipo.id_country.code
          ).subscribe((res: any) => {
            console.log(res);
            alert('Equipo creado correctamente');
          }); 
    }

    update_equipo(){
      this.sle_equipo.photo = this.img_base64;
        this.empleado_service.update_equipo(
            this.sle_equipo.name,
            this.sle_equipo.foundation_date,
            this.sle_equipo.photo,
            this.sle_equipo.id_country.code,
            this.sle_equipo.id
          ).subscribe((res: any) => {
            console.log(res);
            alert('Equipo modificado correctamente');
          });
    }

    delete_equipo(){
        this.empleado_service.delete_equipo(
            this.sle_equipo.id
          ).subscribe((res: any) => {
            console.log(res);
            alert('Estadio eliminado correctamente');
          });
    }

// DIRECTOR TECNICO
    sle_dt = {
        id: 0,
        name: "",
        lastname: "",
        birth_date: "1999-01-01",
        status: { code: 1, name:'Activo'},
        photo: "",
        id_team: { code: 1, name:'Barcelona'},
        id_country: { code: 88, name:'Guatemala'}
    };
    sl_dt = this.country_service.dropdownItems;
    dt_status = [
      { code: 1, name:'Activo'},
      { code: 2, name:'Retirado'},
      { code: 3, name:'Lesionado'}
  ]
    data_dt = [{
        "id"                : 0,
        "name"              : '',
        'lastname'          : '',
        "birth_date"   : '',
        'status' : 1,
        "photo"             : '',
        'id_team': 1,
        'name_team': 'Barcelona',
        "id_country"        : 88,
        "country"           : 'Guatemala',
    }]

    get_dt(){
        this.data_dt = [];
        this.empleado_service.get_dt().subscribe((res: any) => {
            res.data.forEach((element) => {
              this.data_dt.push(element);
            });
          });
    }

    seleccionar_dt(elemento){
      let estado = 'Lesionado';
      if(elemento.status == 1){
        estado = 'Activo';
      }else if(elemento.status == 2){
        estado = 'Retirado';
      }
        this.sle_dt = {
            id: elemento.id,
            name: elemento.name,
            lastname: elemento.lastname,
            birth_date: elemento.birth_date,
            status: { code: elemento.status, name:estado},
            photo: elemento.photo,
            id_team: { code: elemento.id_team, name:elemento.name_team},
            id_country: { code: elemento.id_country, name:elemento.country}
        }
    }

    new_dt(){
      this.sle_dt.photo = this.img_base64;
        this.empleado_service.new_dt(
            this.sle_dt.name,
            this.sle_dt.lastname,
            this.sle_dt.birth_date,
            this.sle_dt.photo,
            this.sle_dt.status.code,
            this.sle_dt.id_country.code
          ).subscribe((res: any) => {
            console.log(res);
            alert('DT creado correctamente');
          }); 
    }

    update_dt(){
      this.sle_dt.photo = this.img_base64;
        this.empleado_service.update_dt(
            this.sle_dt.name,
            this.sle_dt.lastname,
            this.sle_dt.birth_date,
            this.sle_dt.photo,
            this.sle_dt.status.code,
            this.sle_dt.id_country.code,
            this.sle_dt.id
          ).subscribe((res: any) => {
            console.log(res);
            alert('DT modificado correctamente');
          });
    }

    delete_dt(){
        this.empleado_service.delete_dt(
            this.sle_dt.id
          ).subscribe((res: any) => {
            console.log(res);
            alert('DT eliminado correctamente');
          });
    }

    // JUGADOR
    sle_jugador = {
      id: 0,
      name: "",
      lastname: "",
      birth_date: "1999-01-01",
      status: { code: 1, name:'Activo'},
      photo: "",
      position: { code: 1, name:'Barcelona'},
      id_nationality: { code: 88, name:'Guatemala'}
  };
  sl_j = this.country_service.dropdownItems;
  j_status = [
    { code: 1, name:'Activo'},
    { code: 2, name:'Retirado'},
    { code: 3, name:'Lesionado'}
  ]
  j_posicion= [
    { code: 1, name:'Portero'},
    { code: 2, name:'Defensa'},
    { code: 3, name:'Medio'},
    { code: 4, name:'Delantero'}
  ]
  data_j = [{
      "id": 1,
      "name": "Selvin",
      "lastname": "Lisandro",
      "birth_date": "1999-01-01",
      "id_nationality": 88,
      "nationality": "Guatemala",
      "position": 4,
      "status": 1,
      "id_team": 4,
      "name_team" : "Barcelona",
      "photo": "",
  }]

  get_j(){
      this.data_j = [];
      this.empleado_service.get_jugador().subscribe((res: any) => {
          res.data.forEach((element) => {
            this.data_j.push(element);
          });
        });
  }

  seleccionar_j(elemento){
    let estado = 'Lesionado';
    if(elemento.status == 1){
      estado = 'Activo';
    }else if(elemento.status == 2){
      estado = 'Retirado';
    }
    let posicion = "Delantero";
    if(elemento.position == 1){
      posicion = 'Portero';
    }else if(elemento.position == 2){
      posicion = 'Defensa';
    }else if(elemento.position == 3){
      posicion = 'Medio';
    }
      this.sle_jugador = {
          id: elemento.id,
          name: elemento.name,
          lastname: elemento.lastname,
          birth_date: elemento.birth_date,
          status: { code: elemento.status, name:estado},
          photo: elemento.photo,
          position: { code: elemento.position, name:posicion},
          id_nationality: { code: elemento.id_nationality, name:elemento.nationality}
      }
  }

  new_j(){
    this.sle_jugador.photo = this.img_base64;
      this.empleado_service.new_jugador(
          this.sle_jugador.name,
          this.sle_jugador.lastname,
          this.sle_jugador.birth_date,
          this.sle_jugador.id_nationality.code,
          this.sle_jugador.position.code,
          this.sle_jugador.photo,
          this.sle_jugador.status.code
        ).subscribe((res: any) => {
          console.log(res);
          alert('Jugador creado correctamente');
        }); 
  }

  update_j(){
    this.sle_jugador.photo = this.img_base64;
      this.empleado_service.update_jugador(
          this.sle_jugador.name,
          this.sle_jugador.lastname,
          this.sle_jugador.birth_date,
          this.sle_jugador.id_nationality.code,
          this.sle_jugador.position.code,
          this.sle_jugador.photo,
          this.sle_jugador.status.code,
          this.sle_jugador.id
        ).subscribe((res: any) => {
          console.log(res);
          alert('Jugador modificado correctamente');
        });
  }

  delete_j(){
      this.empleado_service.delete_jugador(
          this.sle_jugador.id
        ).subscribe((res: any) => {
          console.log(res);
          alert('Jugador eliminado correctamente');
        });
  }

  // COMPETENCIA
  sle_compe = {
    id: 0,
    name: "",
    type: { code: 2, name:'Eliminatoria'},
    year: 2020,
    id_champion_team: 1,
    id_country: { code: 88, name:'Guatemala'}
  };
  sl_c = this.country_service.dropdownItems;
  c_type = [
    { code: 1, name:'Liga'},
    { code: 2, name:'Eliminatoria'},
    { code: 3, name:'Copa'},
    { code: 4, name:'Super Copa'},
    { code: 5, name:'Cuadrangular'},
    { code: 6, name:'Triangular'}
  ]
  data_c = [{
    "id": 1,
    "name": "BBVA",
    "type": 1,
    "year": 2022,
    "id_champion_team": 1,
    "champion_team": "",
    "id_country": 88,
    "country": "Guatemala"
  }]

get_c(){
    this.data_c = [];
    this.empleado_service.get_compe().subscribe((res: any) => {
        res.data.forEach((element) => {
          this.data_c.push(element);
        });
      });
}

seleccionar_c(elemento){
  let tiponame = "Triangular";
  if(elemento.type == 1){
    tiponame = 'Liga';
  }else if(elemento.type == 2){
    tiponame = 'Eliminatoria';
  }else if(elemento.type == 3){
    tiponame = 'Copa';
  }else if(elemento.type == 4){
    tiponame = 'Super Copa';
  }else if(elemento.type == 5){
    tiponame = 'Cuadrangular';
  }else if(elemento.type == 6){
    tiponame = 'Triangular';
  }
    this.sle_compe = {
        id: elemento.id,
        name: elemento.name,
        type: { code: elemento.type, name:tiponame},
        year: elemento.year,
        id_champion_team: elemento.id_champion_team,
        id_country: { code: elemento.id_country, name:elemento.country}
    }
}

new_c(){
    this.empleado_service.new_compe(
        this.sle_compe.name,
        this.sle_compe.type.code,
        this.sle_compe.year,
        this.sle_compe.id_country.code
      ).subscribe((res: any) => {
        console.log(res);
        alert('Competencia creado correctamente');
      }); 
}

update_c(){
    this.empleado_service.update_compe(
      this.sle_compe.name,
      this.sle_compe.type.code,
      this.sle_compe.year,
      this.sle_compe.id_country.code,
      this.sle_compe.id,
      this.sle_compe.id_champion_team
      ).subscribe((res: any) => {
        console.log(res);
        alert('Competencia modificado correctamente');
      });
}

delete_c(){
    this.empleado_service.delete_compe(
        this.sle_compe.id
      ).subscribe((res: any) => {
        console.log(res);
        alert('Competencia eliminado correctamente');
      });
}

  // PARTIDO
  sle_partido = {
    id: 1,
    game_date: "",
    attendees: 1,
    result_local: 1,
    result_visiting: 2,
    status: { code: 2, name:'Iniciado'},
    id_stadium: 1,
    id_team_local: 1,
    id_team_visiting: 1,
    id_competition: 1
  };
  sl_p = this.country_service.dropdownItems;
  p_status = [
    { code: 1, name:'Sin iniciar'},
    { code: 2, name:'Iniciado'},
    { code: 3, name:'Finalizado'},
    { code: 4, name:'Suspendido'}
  ]
  data_p = [{
      "id": 1,
      "game_date": "1999-01-01",
      "attendees": 1,
      "result_local": 1,
      "result_visiting": 1,
      "status": 1,
      "id_stadium": 1,
      "stadium": "",
      "id_team_local" : 1,
      "team_local": "",
      "id_team_visiting" : 1,
      "team_visiting": "",
      "id_competition": 1,
      "competition": ""
  }]

get_p(){
    this.data_p = [];
    this.empleado_service.get_partido().subscribe((res: any) => {
        res.data.forEach((element) => {
          this.data_p.push(element);
        });
      });
}

seleccionar_p(elemento){
  let tiponame = "Suspendido";
  if(elemento.type == 1){
    tiponame = 'Sin iniciar';
  }else if(elemento.type == 2){
    tiponame = 'Iniciado';
  }else if(elemento.type == 3){
    tiponame = 'Finalizado';
  }
    this.sle_partido = {
      id: elemento.id,
      game_date: elemento.game_date,
      attendees: elemento.attendees,
      result_local: elemento.result_local,
      result_visiting: elemento.result_visiting,
      status: { code: elemento.status, name:tiponame},
      id_stadium: elemento.id_stadium,
      id_team_local: elemento.id_team_local,
      id_team_visiting: elemento.id_team_visiting,
      id_competition: elemento.id_competition
    }
}

new_p(){
    this.empleado_service.new_partido(
      this.sle_partido.game_date,
      this.sle_partido.attendees,
      this.sle_partido.result_local,
      this.sle_partido.result_visiting,
      this.sle_partido.status.code,
      this.sle_partido.id_stadium,
      this.sle_partido.id_team_local,
      this.sle_partido.id_team_visiting,
      this.sle_partido.id_competition
      ).subscribe((res: any) => {
        console.log(res);
        alert('Partido creado correctamente');
      }); 
}

update_p(){
    this.empleado_service.update_partido(
      this.sle_partido.game_date,
      this.sle_partido.attendees,
      this.sle_partido.result_local,
      this.sle_partido.result_visiting,
      this.sle_partido.status.code,
      this.sle_partido.id_stadium,
      this.sle_partido.id_team_local,
      this.sle_partido.id_team_visiting,
      this.sle_partido.id_competition,
      this.sle_partido.id
      ).subscribe((res: any) => {
        console.log(res);
        alert('Partido modificado correctamente');
      });
}

delete_p(){
    this.empleado_service.delete_partido(
        this.sle_partido.id
      ).subscribe((res: any) => {
        console.log(res);
        alert('Partido eliminado correctamente');
      });
}

 // CAMPOS PARA NUEVA IMAGEN
 imagen_user:string = "";
 img_nombre:string = "";
 img_extension:string = "";
 img_base64:any = "";

onUpload(event:any) {
  if(event.target.files) {

    // GET NAME IMAGE
    this.img_nombre = event.target.files[0].name?.toString();
    this.img_nombre = this.img_nombre.replace(/\.[^/.]+$/, "")

    // GET EXTENSION
    this.img_extension = event.target.files[0].name?.toString();
    this.img_extension = this.img_extension.slice((this.img_extension.lastIndexOf(".") - 1 >>> 0) + 2);

    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = ( event2:any ) => {
      this.imagen_user = event2.target.result;
      this.img_base64 = reader.result?.toString();
      this.img_base64 = this.img_base64.replace("data:image/jpeg;base64,", "").replace("data:image/png;base64,", "").replace("data:image/png;base64,", "").replace("data:image/bmp;base64,", "").replace("data:image/raw;base64,", "").replace("data:image/tiff;base64,", "");
    }
  }
}

}
