import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { AppConfig } from 'src/app/api/appconfig';
import { ConfigService } from 'src/app/service/app.config.service';
import { UsuarioService } from 'src/app/service/usuario.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styles:[`
    :host ::ng-deep .p-password input {
    width: 100%;
    padding:1rem;
    }

    :host ::ng-deep .pi-eye{
      transform:scale(1.6);
      margin-right: 1rem;
      color: var(--primary-color) !important;
    }

    :host ::ng-deep .pi-eye-slash{
      transform:scale(1.6);
      margin-right: 1rem;
      color: var(--primary-color) !important;
    }
  `]
})
export class SignupComponent implements OnInit {

  constructor(public router: Router,
    public configService: ConfigService,
    public user_service: UsuarioService) { }

  config: AppConfig;
  subscription: Subscription;
  
  data = {
    name: "",
    lastname: "",
    password: "",
    email: "",
    phone: 0,
    photo: "",
    gender: "M",
    birth_date: "1999-01-01",
    address: "",
    id_country: { code: 88, name:'Guatemala'},
  }

  // CAMPOS PARA NUEVA IMAGEN
  imagen_user:string = "";
  img_nombre:string = "";
  img_extension:string = "";
  img_base64:any = "";

  ngOnInit(): void {
    this.config = this.configService.config;
    this.subscription = this.configService.configUpdate$.subscribe(config => {
      this.config = config;
    });
  }

  onUpload(event:any) {
    if(event.target.files) {

      // GET NAME IMAGE
      this.img_nombre = event.target.files[0].name?.toString();
      this.img_nombre = this.img_nombre.replace(/\.[^/.]+$/, "")

      // GET EXTENSION
      this.img_extension = event.target.files[0].name?.toString();
      this.img_extension = this.img_extension.slice((this.img_extension.lastIndexOf(".") - 1 >>> 0) + 2);

      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = ( event2:any ) => {
        this.imagen_user = event2.target.result;
        this.img_base64 = reader.result?.toString();
        this.img_base64 = this.img_base64.replace("data:image/jpeg;base64,", "").replace("data:image/png;base64,", "").replace("data:image/png;base64,", "").replace("data:image/bmp;base64,", "").replace("data:image/raw;base64,", "").replace("data:image/tiff;base64,", "");
      }
    }
  }

  registrar(){
    this.data.photo = this.img_base64;
    if (
      this.data.name != '' &&
      this.data.lastname != '' &&
      this.data.password != '' &&
      this.data.email != '' &&
      this.data.phone != 0 &&
      this.data.address != '' 
    ){
      this.user_service.registrar_usu(
        this.data.name,
        this.data.lastname,
        this.data.password,
        this.data.email,
        this.data.phone,
        this.data.photo,
        this.data.gender,
        this.data.birth_date,
        this.data.address,
        this.data.id_country.code
      ).subscribe((res: any) => {
        alert(res.message)
        console.log(res);
        this.router.navigate(['/pages/login']);
      });
    }else{
      alert("Existen campos incompletos")
      this.router.navigate(['/pages/signup']);
    }
    
  }

  regresar(){
    this.router.navigate(['/pages/login']);
  }

  dropdownItems = [
    { code: 1, name:'Afghanistan'},
    { code: 2, name:'Albania'},
    { code: 3, name:'Algeria'},
    { code: 4, name:'American Samoa'},
    { code: 5, name:'Andorra'},
    { code: 6, name:'Angola'},
    { code: 7, name:'Anguilla'},
    { code: 8, name:'Antarctica'},
    { code: 9, name:'Antigua and Barbuda'},
    { code: 10, name:'Argentina'},
    { code: 11, name:'Armenia'},
    { code: 12, name:'Aruba'},
    { code: 13, name:'Australia'},
    { code: 14, name:'Austria'},
    { code: 15, name:'Azerbaijan'},
    { code: 16, name:'Bahamas'},
    { code: 17, name:'Bahrain'},
    { code: 18, name:'Bangladesh'},
    { code: 19, name:'Barbados'},
    { code: 20, name:'Belarus'},
    { code: 21, name:'Belgium'},
    { code: 22, name:'Belize'},
    { code: 23, name:'Benin'},
    { code: 24, name:'Bermuda'},
    { code: 25, name:'Bhutan'},
    { code: 26, name:'Bolivia'},
    { code: 27, name:'Bosnia and Herzegovina'},
    { code: 28, name:'Botswana'},
    { code: 29, name:'Bouvet Island'},
    { code: 30, name:'Brazil'},
    { code: 31, name:'British Indian Ocean Territory'},
    { code: 32, name:'Brunei Darussalam'},
    { code: 33, name:'Bulgaria'},
    { code: 34, name:'Burkina Faso'},
    { code: 35, name:'Burundi'},
    { code: 36, name:'Cambodia'},
    { code: 37, name:'Cameroon'},
    { code: 38, name:'Canada'},
    { code: 39, name:'Cape Verde'},
    { code: 40, name:'Cayman Islands'},
    { code: 41, name:'Central African Republic'},
    { code: 42, name:'Chad'},
    { code: 43, name:'Chile'},
    { code: 44, name:'China'},
    { code: 45, name:'Christmas Island'},
    { code: 46, name:'Cocos { code: Keeling} Islands'},
    { code: 47, name:'Colombia'},
    { code: 48, name:'Comoros'},
    { code: 49, name:'Congo'},
    { code: 50, name:'Congo, name:the Democratic Republic of the'},
    { code: 51, name:'Cook Islands'},
    { code: 52, name:'Costa Rica'},
    { code: 53, name:'Cote DIvoire'},
    { code: 54, name:'Croatia'},
    { code: 55, name:'Cuba'},
    { code: 56, name:'Cyprus'},
    { code: 57, name:'Czech Republic'},
    { code: 58, name:'Denmark'},
    { code: 59, name:'Djibouti'},
    { code: 60, name:'Dominica'},
    { code: 61, name:'Dominican Republic'},
    { code: 62, name:'Ecuador'},
    { code: 63, name:'Egypt'},
    { code: 64, name:'El Salvador'},
    { code: 65, name:'Equatorial Guinea'},
    { code: 66, name:'Eritrea'},
    { code: 67, name:'Estonia'},
    { code: 68, name:'Ethiopia'},
    { code: 69, name:'Falkland Islands { code: Malvinas}'},
    { code: 70, name:'Faroe Islands'},
    { code: 71, name:'Fiji'},
    { code: 72, name:'Finland'},
    { code: 73, name:'France'},
    { code: 74, name:'French Guiana'},
    { code: 75, name:'French Polynesia'},
    { code: 76, name:'French Southern Territories'},
    { code: 77, name:'Gabon'},
    { code: 78, name:'Gambia'},
    { code: 79, name:'Georgia'},
    { code: 80, name:'Germany'},
    { code: 81, name:'Ghana'},
    { code: 82, name:'Gibraltar'},
    { code: 83, name:'Greece'},
    { code: 84, name:'Greenland'},
    { code: 85, name:'Grenada'},
    { code: 86, name:'Guadeloupe'},
    { code: 87, name:'Guam'},
    { code: 88, name:'Guatemala'},
    { code: 89, name:'Guinea'},
    { code: 90, name:'Guinea-Bissau'},
    { code: 91, name:'Guyana'},
    { code: 92, name:'Haiti'},
    { code: 93, name:'Heard Island and Mcdonald Islands'},
    { code: 94, name:'Holy See { code: Vatican City State}'},
    { code: 95, name:'Honduras'},
    { code: 96, name:'Hong Kong'},
    { code: 97, name:'Hungary'},
    { code: 98, name:'Iceland'},
    { code: 99, name:'India'},
    { code: 100, name:'Indonesia'},
    { code: 101, name:'Iran, name:Islamic Republic of'},
    { code: 102, name:'Iraq'},
    { code: 103, name:'Ireland'},
    { code: 104, name:'Israel'},
    { code: 105, name:'Italy'},
    { code: 106, name:'Jamaica'},
    { code: 107, name:'Japan'},
    { code: 108, name:'Jordan'},
    { code: 109, name:'Kazakhstan'},
    { code: 110, name:'Kenya'},
    { code: 111, name:'Kiribati'},
    { code: 112, name:'Korea, name:Democratic Peoples Republic of'},
    { code: 113, name:'Korea, name:Republic of'},
    { code: 114, name:'Kuwait'},
    { code: 115, name:'Kyrgyzstan'},
    { code: 116, name:'Lao Peoples Democratic Republic'},
    { code: 117, name:'Latvia'},
    { code: 118, name:'Lebanon'},
    { code: 119, name:'Lesotho'},
    { code: 120, name:'Liberia'},
    { code: 121, name:'Libyan Arab Jamahiriya'},
    { code: 122, name:'Liechtenstein'},
    { code: 123, name:'Lithuania'},
    { code: 124, name:'Luxembourg'},
    { code: 125, name:'Macao'},
    { code: 126, name:'Macedonia, name:the Former Yugoslav Republic of'},
    { code: 127, name:'Madagascar'},
    { code: 128, name:'Malawi'},
    { code: 129, name:'Malaysia'},
    { code: 130, name:'Maldives'},
    { code: 131, name:'Mali'},
    { code: 132, name:'Malta'},
    { code: 133, name:'Marshall Islands'},
    { code: 134, name:'Martinique'},
    { code: 135, name:'Mauritania'},
    { code: 136, name:'Mauritius'},
    { code: 137, name:'Mayotte'},
    { code: 138, name:'Mexico'},
    { code: 139, name:'Micronesia, name:Federated States of'},
    { code: 140, name:'Moldova, name:Republic of'},
    { code: 141, name:'Monaco'},
    { code: 142, name:'Mongolia'},
    { code: 143, name:'Montserrat'},
    { code: 144, name:'Morocco'},
    { code: 145, name:'Mozambique'},
    { code: 146, name:'Myanmar'},
    { code: 147, name:'Namibia'},
    { code: 148, name:'Nauru'},
    { code: 149, name:'Nepal'},
    { code: 150, name:'Netherlands'},
    { code: 151, name:'Netherlands Antilles'},
    { code: 152, name:'New Caledonia'},
    { code: 153, name:'New Zealand'},
    { code: 154, name:'Nicaragua'},
    { code: 155, name:'Niger'},
    { code: 156, name:'Nigeria'},
    { code: 157, name:'Niue'},
    { code: 158, name:'Norfolk Island'},
    { code: 159, name:'Northern Mariana Islands'},
    { code: 160, name:'Norway'},
    { code: 161, name:'Oman'},
    { code: 162, name:'Pakistan'},
    { code: 163, name:'Palau'},
    { code: 164, name:'Palestinian Territory, name:Occupied'},
    { code: 165, name:'Panama'},
    { code: 166, name:'Papua New Guinea'},
    { code: 167, name:'Paraguay'},
    { code: 168, name:'Peru'},
    { code: 169, name:'Philippines'},
    { code: 170, name:'Pitcairn'},
    { code: 171, name:'Poland'},
    { code: 172, name:'Portugal'},
    { code: 173, name:'Puerto Rico'},
    { code: 174, name:'Qatar'},
    { code: 175, name:'Reunion'},
    { code: 176, name:'Romania'},
    { code: 177, name:'Russian Federation'},
    { code: 178, name:'Rwanda'},
    { code: 179, name:'Saint Helena'},
    { code: 180, name:'Saint Kitts and Nevis'},
    { code: 181, name:'Saint Lucia'},
    { code: 182, name:'Saint Pierre and Miquelon'},
    { code: 183, name:'Saint Vincent and the Grenadines'},
    { code: 184, name:'Samoa'},
    { code: 185, name:'San Marino'},
    { code: 186, name:'Sao Tome and Principe'},
    { code: 187, name:'Saudi Arabia'},
    { code: 188, name:'Senegal'},
    { code: 189, name:'Serbia and Montenegro'},
    { code: 190, name:'Seychelles'},
    { code: 191, name:'Sierra Leone'},
    { code: 192, name:'Singapore'},
    { code: 193, name:'Slovakia'},
    { code: 194, name:'Slovenia'},
    { code: 195, name:'Solomon Islands'},
    { code: 196, name:'Somalia'},
    { code: 197, name:'South Africa'},
    { code: 198, name:'South Georgia and the South Sandwich Islands'},
    { code: 199, name:'Spain'},
    { code: 200, name:'Sri Lanka'},
    { code: 201, name:'Sudan'},
    { code: 202, name:'Suriname'},
    { code: 203, name:'Svalbard and Jan Mayen'},
    { code: 204, name:'Swaziland'},
    { code: 205, name:'Sweden'},
    { code: 206, name:'Switzerland'},
    { code: 207, name:'Syrian Arab Republic'},
    { code: 208, name:'Taiwan, name:Province of China'},
    { code: 209, name:'Tajikistan'},
    { code: 210, name:'Tanzania, name:United Republic of'},
    { code: 211, name:'Thailand'},
    { code: 212, name:'Timor-Leste'},
    { code: 213, name:'Togo'},
    { code: 214, name:'Tokelau'},
    { code: 215, name:'Tonga'},
    { code: 216, name:'Trinidad and Tobago'},
    { code: 217, name:'Tunisia'},
    { code: 218, name:'Turkey'},
    { code: 219, name:'Turkmenistan'},
    { code: 220, name:'Turks and Caicos Islands'},
    { code: 221, name:'Tuvalu'},
    { code: 222, name:'Uganda'},
    { code: 223, name:'Ukraine'},
    { code: 224, name:'United Arab Emirates'},
    { code: 225, name:'United Kingdom'},
    { code: 226, name:'United States'},
    { code: 227, name:'United States Minor Outlying Islands'},
    { code: 228, name:'Uruguay'},
    { code: 229, name:'Uzbekistan'},
    { code: 230, name:'Vanuatu'},
    { code: 231, name:'Venezuela'},
    { code: 232, name:'Viet Nam'},
    { code: 233, name:'Virgin Islands, name:British'},
    { code: 234, name:'Virgin Islands, name:U.s.'},
    { code: 235, name:'Wallis and Futuna'},
    { code: 236, name:'Western Sahara'},
    { code: 237, name:'Yemen'},
    { code: 238, name:'Zambia'},
    { code: 239, name:'Zimbabwe'}
   ];
}
