import { Component, OnInit } from '@angular/core';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import { AdministrativosService } from 'src/app/service/administrativos.service';
import { CountryService } from 'src/app/service/countryservice';
import { UsuarioService } from 'src/app/service/usuario.service';


@Component({
  selector: 'app-admin-reportes',
  templateUrl: './admin-reportes.component.html',
  styleUrls: ['./admin-reportes.component.scss']
})
export class AdminReportesComponent implements OnInit {

  constructor(
    public user_service: UsuarioService,
    public country_service: CountryService,
    public empleado_service: AdministrativosService
  ) { }

  ngOnInit(): void {
    this.get_equipo();
    this.get_reporte3();
    this.get_reporte4();
  }

  data_reportes = [
    { code: 'r1', name:'Usuarios Suscritos a X equipo'},
    { code: 'r2', name:'Usuarios con o sin membresia '},
    { code: 'r3', name:'Usuarios que Mas membresías han adquirido'},
    { code: 'r4', name:'Usuarios que más dinero han gastado'},
    { code: 'r5', name:'Usuarios de x pais'},
    { code: 'r6', name:'Usuarios de x genero'},
    { code: 'r7', name:'Usuarios con al menos x años de edad'},
    { code: 'r8', name:'Empleados que MAS/MENOS noticias han publicado'},
    { code: 'r9', name:'Empleados que MAS/MENOS noticias han publicado de X Equipo'}
  ];
  sle_rp_pdf = { code: 'r1', name:'Usuarios Suscritos a X equipo'};

  data_equipo = [
    { code: 4, name:'Barcelona'}
  ]
  //false = jugadores
  //true = DT
  DTjugadores= false;

  get_equipo(){
    this.data_equipo = [];
    this.empleado_service.get_equipos().subscribe((res: any) => {
        res.data.forEach((element) => {
          this.data_equipo.push({code: element.id, name: element.name});
        });
      });
  }

  data_r1 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'nationality': '',
    'photo': ''
  }];
  Seleccionado1 = { code: 2, name:'Sin membresia'};

  

  get_reporte1(){
    this.data_r1 = [];
    let codigo;
    codigo = this.Seleccionado2.code == 1 ? true : false;
    this.user_service.reporte1(this.Seleccionado1.code,codigo).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r1.push(element);
      });
    });
  }

  data_r2 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'nationality': '',
    'photo': ''
  }];
  sl_r2 = [
    { code: 1, name:'Con membresia'},
    { code: 2, name:'Sin membresia'}
  ];
  Seleccionado2 = { code: 2, name:'Sin membresia'};
  get_reporte2(){
    this.data_r2 = [];
    this.user_service.get_reporte2(this.Seleccionado2.code == 1 ? true : false).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r2.push(element);
      });
    });
  }

  data_r3 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'nationality': '',
    'photo': '',
    'count': 0
  }];
  get_reporte3(){
    this.data_r3 = [];
    this.user_service.get_reporte3().subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r3.push(element);
      });
    });
  }

  data_r4 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'nationality': '',
    'photo': '',
    'amount': 0
  }];
  get_reporte4(){
    this.data_r4 = [];
    this.user_service.get_reporte4().subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r4.push(element);
      });
    });
  }

  data_r5 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'photo': ''
  }];
  sl_r5 = this.country_service.dropdownItems;
  Seleccionado5 = { code: 38, name:'Canada'};
 
  get_reporte5(){
    this.data_r5 = [];
    this.user_service.get_reporte5(this.Seleccionado5.code).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r5.push(element);
      });
    });
  }

  data_r6 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'photo': '',
    'nationality': ''
  }];
  sl_r6 = [
    { code: 1, name:'Hombre'},
    { code: 2, name:'Mujer'}
  ];
  Seleccionado6 = { code: 1, name:'Hombre'};
 
  get_reporte6(){
    this.data_r6 = [];
    this.user_service.get_reporte6(this.Seleccionado6.code == 1 ? 'M' : 'F').subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r6.push(element);
      });
    });
  }

  data_r7 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'photo': '',
    'nationality': '',
    'birth_date': '',
    'age': 20
  }];
  Seleccionado7 = 20;
 
  get_reporte7(){
    this.data_r7 = [];
    this.user_service.get_reporte7(this.Seleccionado7).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r7.push(element);
      });
    });
  }

  data_r8 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'photo': '',
    'nationality': '',
    'count': 20
  }];
 
  get_reporte8(){
    this.data_r8 = [];
    this.user_service.get_reporte8(0).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r8.push(element);
      });
    });
  }
  get_reporte8_menos(){
    this.data_r8 = [];
    this.user_service.get_reporte8(1).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r8.push(element);
      });
    });
  }

  data_r9 = [{
    'id': 3,
    'name': '',
    'lastname': '',
    'photo': '',
    'nationality': '',
    'count': 20
  }];
  Seleccionado9 = { code: 4, name:'Barcelona'};
  get_reporte9(){
    this.data_r9 = [];
    this.user_service.get_reporte9(0, this.Seleccionado9.code).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r9.push(element);
      });
    });
  }
  get_reporte9_menos(){
    this.data_r9 = [];
    this.user_service.get_reporte9(1, this.Seleccionado9.code).subscribe((res: any) => {
      res.data.forEach((element) => {
        this.data_r9.push(element);
      });
    });
  }

  descargarpdf(){
    var datapdf = document.getElementById(this.sle_rp_pdf.code);
    html2canvas(datapdf).then((canvas) => {
      var imgWidth = 208;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png')
      let pdf = new jsPDF('p', 'mm', 'a4');
      var position = 0;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf.save('G_E.pdf');
    })
  }

}
