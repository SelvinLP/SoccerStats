import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppMainComponent } from './app.main.component';
import { UsuarioService } from './service/usuario.service';

@Component({
    selector: 'app-menu',
    template: `
        <div class="layout-menu-container">
            <ul class="layout-menu" role="menu" (keydown)="onKeydown($event)">
                <li app-menu class="layout-menuitem-category" *ngFor="let item of model; let i = index;" [item]="item" [index]="i" [root]="true" role="none">
                    <div class="layout-menuitem-root-text" [attr.aria-label]="item.label">{{item.label}}</div>
                    <ul role="menu">
                        <li app-menuitem *ngFor="let child of item.items" [item]="child" [index]="i" role="none"></li>
                    </ul>
                </li>
            </ul>
        </div>
    `
})
export class AppMenuComponent implements OnInit {

    model: any[];

    constructor(
        public appMain: AppMainComponent,
        public usu_service: UsuarioService,
        public router: Router) { }

    ngOnInit() {
        if(this.usu_service.rol == 'Cliente'){
            this.model = [
                {
                    label: 'Inicio',
                    items: [
                        {label: 'Dashboard Reportes',icon: 'pi pi-fw pi-chart-bar', routerLink: ['/pages/inicio/user-reportes']},
                        {label: 'Noticias',icon: 'pi pi-fw pi-comment', routerLink: ['/pages/inicio/user-reportes']}
                    ]
                }
            ];
        }else if(this.usu_service.rol == 'Empleado'){
            this.model = [
                {
                    label: 'Inicio',
                    items: [
                        {label: 'CRUD de datos', icon: 'pi pi-fw pi-circle', routerLink: ['/pages/inicio/uikit/misc']},
                        {label: 'Otras acciones', icon: 'pi pi-fw pi-list', routerLink: ['/pages/inicio/uikit/list']},
                    ]
                }
            ];
        }else if(this.usu_service.rol == 'Administrador'){
            this.model = [
                {
                    label: 'Inicio',
                    items:[
                        {label: 'Control de Usuarios',icon: 'pi pi-fw pi-home', routerLink: ['/pages/inicio/admin-crud']},
                        {label: 'Reportes',icon: 'pi pi-fw pi-home', routerLink: ['/pages/inicio/admin-reportes']},
                        {label: 'Bitacora',icon: 'pi pi-fw pi-home', routerLink: ['/pages/inicio/admin-bitacora']}
                    ]
                },
            ]
        }else{
            this.router.navigate(['/pages/access']);
        }
        
    }

    onKeydown(event: KeyboardEvent) {
        const nodeElement = (<HTMLDivElement> event.target);
        if (event.code === 'Enter' || event.code === 'Space') {
            nodeElement.click(); 
            event.preventDefault();
        }
    }
}
