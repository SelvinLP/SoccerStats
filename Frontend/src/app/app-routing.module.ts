import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormLayoutComponent } from './components/formlayout/formlayout.component';
import { PanelsComponent } from './components/panels/panels.component';
import { OverlaysComponent } from './components/overlays/overlays.component';
import { MessagesComponent } from './components/messages/messages.component';
import { MiscComponent } from './components/misc/misc.component';
import { EmptyComponent } from './components/empty/empty.component';
import { ChartsComponent } from './components/charts/charts.component';
import { AppMainComponent } from './app.main.component';
import { InputComponent } from './components/input/input.component';
import { ButtonComponent } from './components/button/button.component';
import { TableComponent } from './components/table/table.component';
import { ListComponent } from './components/list/list.component';
import { CrudComponent } from './components/crud/crud.component';
import { LoginComponent } from './components/login/login.component';
import { ErrorComponent } from './components/error/error.component';
import { NotfoundComponent } from './components/notfound/notfound.component';
import { AccessComponent } from './components/access/access.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { SignupComponent } from './components/signup/signup.component';
import { ProfileComponent } from './components/profile/profile.component';
import { AdminCrudComponent } from './components/admin-crud/admin-crud.component';
import { AdminReportesComponent } from './components/admin-reportes/admin-reportes.component';
import { AdminBitacoraComponent } from './components/admin-bitacora/admin-bitacora.component';
import { UserReportesComponent } from './components/user-reportes/user-reportes.component';
@NgModule({
    imports: [
        RouterModule.forRoot([
            {path:'pages/login', component: LoginComponent},
            {path:'pages/signup', component: SignupComponent},
            {path:'pages/profile', component: ProfileComponent},
            {path:'', component: InicioComponent},
            {
                path: 'pages/inicio', component: AppMainComponent,
                children: [
                    {path: 'dashboard', component: DashboardComponent},
                    {path: 'uikit/list', component: ListComponent},
                    {path: 'uikit/misc', component: MiscComponent},
                    {path: 'admin-crud', component: AdminCrudComponent},
                    {path: 'admin-reportes', component: AdminReportesComponent},
                    {path: 'admin-bitacora', component: AdminBitacoraComponent},
                    {path: 'user-reportes', component: UserReportesComponent}
                ],
            },
            {path:'pages/error', component: ErrorComponent},
            {path:'pages/notfound', component: NotfoundComponent},
            {path:'pages/access', component: AccessComponent},
            {path: '**', redirectTo: 'pages/notfound'},
        ], {scrollPositionRestoration: 'enabled', anchorScrolling:'enabled'})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
