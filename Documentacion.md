
# SoccerStats

## Indice

1. [Breve resumen de la aplicación a desarrollar](#resumen)
    1. [Funcionalidad](#funcionalidad)
2. [Versionamiento del documento](#versionamiento)
3. [Lenguaje de programación](#lenguaje)
	1. [Frontend](#frontend)	
    2. [Backend](#backend)
4. [Herramientas de desarrollo a utilizar](#desarrollo)
    1. [Docker](#docker)
    2. [JWT](#jwt)
    3. [GitLab ci/cd](#gitcicd)
    4. [Ansible](#ansible)
5. [Herramientas de metodología a utilizar](#metodologia)
	1. [Scrum](#scrum)
6. [Herramientas de control de tiempo de trabajo](#control)
	1. [Kanvan, Gitlab, Trello](#kanvan)
7. [Pruebas a implementar](#pruebas)
	1. [Pruebas unitarias](#unitarias)
	2. [Pruebas funcionales](#funcionales)
8. [Arquitectura a implementar](#arquitectura)
	1. [Arquitectura de Microservicios](#microservicios)
9. [Maquetacion](#maquetacion)
10. [Algoritmo de prediccion](#algoritmo-de-prediccion)
10. [Diagramas de diseño](#diseno)
11. [Video de solución SoccerStats](#video)


## Breve resumen de la aplicación a desarrollar <a name="resumen"></a>

Soccer Stats será una plataforma diseñada y personalizada para todos los usuarios aficionados al futbol que deseen verificar datos estadísticos a través del tiempo.

### Funcionalidades <a name="funcionalidad"></a>

- Existira un login para definir el tipo de usuario que ingresa al sitio web.
- Los empleados podran ingresar y modificar los datos de las predicciones. Asi como ingresos de nuevas noticias.
- Se podra registrar nuevos usuarios al sistema, los clientes se registrarán desde un formulario en la web y los otros tipos de usuario solo los podra crear el administrador.
- Los clientes podran modificar su propia informacion sin tomar en cuenta el correo electronico.
- Los clientes sin membresia podran ver la informacion de los partidos, filtrarlos. En caso de que no tenga membresia podrá adquirirla con un precio de Q 15.00 mensuales. 
- Los clientes con membresia podran seguir a sus equipos favoritos, podran realiza consultas de datos estadisticos, podran filtrar las noticias de sus equipos y finalmente podra tener acceso al algoritmo de prediccion.
- Existira una aplicacion movil que podran navegar por las funcionalidades de la misma manera que en la web con la ventaja que existiran notificaciones.

## Versionamiento del documento <a name="versionamiento"></a>

A continuacion se explicara las razones de las tecnologia y metodolofia utilizadas.

- **Gitlab:** 

Gitlab es un servicio web de control de versiones y desarrollo de software colaborativo basado en Git. Además de gestor de repositorios, el servicio ofrece también alojamiento de wikis y un sistema de seguimiento de errores, todo ello publicado bajo una Licencia de código abierto.

Utilizamos gitlab por las siguientes razones.

1. Plan gratuito y sin limitaciones, aunque tiene planes de pago.

2. Es de licencia de código abierto.

3. Permite el autohospedaje en cualquier plan.

4. Está muy bien integrado con Git.

- **Gitflow:** 

Gitflow es un modelo alternativo de creación de ramas en Git en el que se utilizan ramas de función y varias ramas principales. En comparación con el desarrollo basado en troncos, Gitflow tiene diversas ramas de más duración y mayores confirmaciones. 

Utilizamos gitflow por las siguientes razones.

1. Aumenta la velocidad de entrega de código terminado al equipo de pruebas.

2. Disminuyen los errores humanos en la mezcla de las ramas.

3. Elimina la dependencia de funcionalidades al momento de entregar código para ser puesto en producción.

Estas ramas de función de larga duración requieren más colaboración para la fusión y tienen mayor riesgo de desviarse de la rama troncal. También pueden introducir actualizaciones conflictivas.

## Lenguaje de programación <a name="lenguaje"></a>

### Frontend  <a name="frontend"></a>

El frontend es la parte del desarrollo web que se dedica a la parte frontal de un sitio web, en pocas palabras del diseño de un sitio web, desde la estructura del sitio hasta los estilos como colores, fondos, tamaños hasta llegar a las animaciones y efectos. 

Para el desarrollo del frontend se utilizaran las siguientes herramientas.

- **Angular:** 

Angular es un framework de código abierto desarrollado por el gigante de la computación Google, que facilita el diseño y codificación de aplicaciones web de mediana y alta complejidad. De hecho, una de las ventajas de Angular es que permite desarrollar aplicaciones en Windows, Linux y Mac, compatible con una serie de API nativas para esos entornos. Es decir, Angular permite integrar dos aplicaciones o servicios, lo que facilita incorporar nuevas funcionalidades al software.

Version de angular a utilizar: 12.1.4

- **Bootstrap:** 

Bootstrap es un framework CSS y Javascript diseñado para la creación de interfaces limpias y con un diseño responsive. Entre las ventajas de usar Bootstrap es tener una web bien organizada de forma visual rápidamente: la curva de aprendizaje hace que su manejo sea asequible y rápido si ya sabes maquetar. Permite utilizar muchos elementos web: desde iconos a desplegables, combinando HTML5, CSS y Javascript.

Version de bootstrap a utilizar: 5.0.2 

### Backend  <a name="backend"></a>

Es la capa de acceso a los datos, ya sea de un software o de un dispositivo en general, es la lógica tecnológica que hace que una página web funcione, lo que queda oculto a ojos del visitante. El backend de una solución, determina qué tan bien se ejecutará la aplicación y qué experiencia, positiva o negativa, obtendrá el usuario de su uso.

- **Node js:** 

Node.js es un entorno controlado por eventos diseñado para crear aplicaciones escalables, permitiéndote establecer y gestionar múltiples conexiones al mismo tiempo. Gracias a esta característica, no tienes que preocuparte con el bloqueo de procesos, pues no hay bloqueos.

Version de nodejs a utilizar: 16.6.1

- **Python:** 

Python es un lenguaje de scripting independiente de plataforma y orientado a objetos, preparado para realizar cualquier tipo de programa, desde aplicaciones Windows a servidores de red o incluso, páginas web. Es un lenguaje interpretado, lo que significa que no se necesita compilar el código fuente para poder ejecutarlo, lo que ofrece ventajas como la rapidez de desarrollo e inconvenientes como una menor velocidad.

Este principalmente se usaran para los algoritmos de prediccion. 

Version de python a utilizar: 3.10.2​

## Herramientas de desarrollo a utilizar <a name="desarrollo"></a>

- **Docker:**  <a name="docker"></a>

Docker es una herramienta de software que permite crear, probar e implementar aplicaciones que empaqueta en unidades llamadas contenedores con todo lo necesario para que el software se ejecute, y elimina la necesidad de administrar directamente, proporcionando comandos sencillos que pueden utilizarse para crear, iniciar o detener dichos contenedores.

En el proyecto SoccerStats se utilizara por las ventajas que proporciona como la rapidez con la que permite entregar los servicios y con la frecuencia necesaria, y el estandar en las aplicaciones para la implementación y la facilidad de identificación de problemas, y también proporciona sencillez en la transferencia entre el desarrollo local y la implementacion en producción, y mejorando el desarrollo y utilización de software variado que conlleva al ahorro de dinero.

Version de docker a utilizar:

Se utilizara la version mas actualizada y estable de docker en este caso la version 20.10.12

- **JWT:** <a name="jwt"></a>

JSON Web Token define un método para encapsular y compartir aserciones sobre una entidad de manera segura entre distintas partes mediante el uso de objetos JSON.
Los tokens generados no están cifrados, y la cadena que devuelve es una serialización usando codificación base64url, que puede decodificarse fácilmente para ver el contenido JSON del token.

Tokens a utilizar en el proyecto SoccerStats:

1. Data token: Es una serialización en JWT compacta y fácil de transmitir para peticiones HTTP y poder realizar el intercambio de datos.

2. ID token: Es generado para la petición de una aplicación cliente, cuando un usuario se loguea le permite a la aplicación cliente obtener datos del usuario de manera confiable sin tener que gestionar sus credenciales.

3. Access token: Es un token que realiza una petición de una aplicación cliente, y permite a este el acceso a un recurso protegido en nombre de un usuario. Este token se usa como método de autenticación y autorización por parte de la aplicación cliente frente al servidor que aloja el recurso.

La version a utilizar en el proyecto SoccerStats sera: 8.9.0

- **Gitlab Ci/Cd:** <a name="gitcicd"></a>

Se utilizará Gitlab para la integración continua que es la práctica de integrar los cambios de código en la rama principal de un repositorio de código fuente, se probara automáticamente cada cambio cuando los cambios se confirman o se fusionan, e iniciando automáticamente una compilación. Con la integración continua, los errores y los problemas de seguridad se pueden identificar y corregir más fácilmente y mucho antes en el ciclo de vida del desarrollo de software.

Es necesario la creación de pipelines para la estructura de datos abstracta generados desde un archivo YAML para que luego la convierte en modelos persistentes por ejemplo una canalización, etapas y trabajos.

Después de eso, la canalización está lista para ser procesada, eso significa ejecutar los trabajos en orden de ejecución o etapas hasta cualquiera de los siguientes puntos:

Se han ejecutado todos los trabajos esperados.
Los errores interrumpiran la ejecución de la canalización.

Finalmente se si ha salido correctamente correra las imagenes docker en los servidores de desarrollo y seguidamente el servidor de producción.


- **Ansible:** <a name="ansible"></a>

Esta herramienta de software ayuda en la gestión de la configuración automática y remota, el cual nos permite centralizar la configuración de dispositivos de una forma sencilla y automatizada.

Utilización en el proyecto SoccerStats

1. Se realizará la instalación de Ansible con la herramienta docker en un contenedor.

2. Se realizará la configuración de los servidores de servicios apta para el despliegue.


## Herramientas de metodología a utilizar <a name="metodologia"></a>

### Scrum

La herramienta de metodologia que se implementa para el desarrollo del proyecto es el marco de trabajo denominado "Scrum" el cual es un proceso de gestion, que reduce la complejidad en el desarrollo de productos para satisfacer las necesidades de los clientes. La gerencia y los equipos de Scrum trabajan juntos alrededor de requisitos y tecnologías para entregar productos funcionando de manera incremental usando el empirismo.

Entre los eventos Scrum que se utilizan en el desarrollo del proyecto son los siguientes:

- **Sprints:** 
  Para la implementacion de la solucion se cuenta con 5 sprints, estos sprints tienen una duracion aproximada de 2 semanas cada uno. Teniendo unos dias de holgura adicionales por cualquier cuestion que se pudiera presentar.
  
- **Sprint Planning:**
  La planificacion del sprint se realiza con al menos 2 dias de anticipacion previo al inicio de cada uno de estos. En esta planificacion participan todos los miembros del equipo. En la planificacion del sprint se asignan las tareas al mismo, y a los miembros del equipo.
  
- **Sprint Review:**
  La revision del sprint se realiza el dia que concluye el sprint. En esta revision se mide principalmente el cumplimiento de las tareas asignadas en el sprint. 
  
- **Sprint Retrospective:**
  La restrospectiva del sprint se realiza en paralelo con la revision del sprint. En esta retrospectiva se analizan aspectos negativos que afectaron el rendimiento durante el sprint y que pueden mejorarse o evitarse para el siguiente sprint.

Entre los artefactos Scrum que se utilizan en el desarrollo del proyecto son los siguientes:

- **Product backlog:**
  El product backlog es el listado de completo de tareas del proyecto. Estas tareas se producen en el analisis y toma de requerimientos del producto. Estas tareas se asignan al sprint backlog conforme van avanzando los sprints.

- **Sprint backlog:**
  El sprint backlog es el listado de tareas del sprint. Estas tareas se van modificando y eliminando del listado conforme se avanza con el desarrollo del proyecto y los sprints.

- **Tablero Kanban:**
  El tablero Kanban es una herramienta que sirve para tener a la vista las distintas tareas que se manejan separado por secciones. Dentro de este tablero se puede visualizar el producto backlog, el sprint backlog, ademas se incluye un listado de tareas en proceso y un listado de tareas terminada. Para la utilizacion de este tablero se utiliza la herramienta de Trello.

## Herramientas de control de tiempo de trabajo <a name="control"></a>

Se utilizará un método visual para gestionar y procesar el tiempo de trabajo en equipo con el objetivo de poder visualizar el trabajo, limitar acumulación de tareas y maximizar la eficiencia y el flujo de trabajo en equipo, y reducir la duración del proyecto con la intervención de cada miembro.


- **Trello:**

Al igual que el tablero de Gitlab, Trello ayuda a la organización, ya que permiten gestionar, supervisar y compartir las tareas de principio a fin. Utilizando tarjetas para descubrir un ecosistema de checklists, fechas de vencimiento, archivos adjuntos, conversaciones y mucho más.

Se utilizara para llevar el control y respaldo de Gitlab en cazo de fallos o inconvenientes.

Se creara un tablero con las siguientes listas:

1. Lista Backlog: Lista todas las tareas del proyecto.

2. Lista haciendo: Lista las tareas que se estan realizando.

3. Lista hecho: Lista las tareas terminadas.


Cuando un hito o tarea se mueve de una lista a otra en el tablero, el tablero realizará dos cosas:

1. Elimina la etiqueta, hito o asignación de la lista de inicio.

2. Agrega la etiqueta, el hito o la asignación de la lista a la que se agrega el problema.


## Pruebas a implementar <a name="pruebas"></a>

- **Pruebas unitarias:**    <a name="unitarias"></a>

Las pruebas unitarias son una técnica para probar un pequeño bloque de código independiente, este código de bloque pequeño puede ser una función en la mayoría de los casos que no depende de otras piezas de código del proyecto.

Se deben ejecutar durante el ciclo de DevOps y notificar sobre posibles fallos.

### Herramientas:

#### Python unittest

Permite ejecutar las pruebas sin mucho esfuerzo, podemos usar pruebas de unidad de módulo para probar sin utilizar los módulos de terceros.

Pasos para la utilización de unittest:

1. Escribir metodos y funciones.

2. Importar el modulo unittest.

3. Crear un archivo que comience con la palabra clave test que se utiliza para identificar los archivos de prueba.

4. Crear una clase ampliando la clase unittest.TestCase.

5. Escribir métodos para las pruebas dentro de la clase donde cada método contiene diferentes casos de prueba según los requisitos.

6. Ejecutar las pruebas con el comando directo o ejecutando un archivo invocando el metodo main del modulo unittest.


#### Mocha 

Este es un framework de pruebas para NodeJS el cual nos permite ejecutar las pruebas desde la consola o desde un navegador. Tambien permite realizar pruebas asíncronas de manera sencilla.

#### Chai 

Chai es una librería de aserciones para NodeJS y/o navegador, que puede ser emparejado con cualquier framework Javascript.

como una extensión de la librería CHAI, que permite realizar pruebas de integración con llamadas HTTP utilizando las aserciones de CHAI y todos los métodos de HTTP como GET, POST, PUT, DELETE.



- **Pruebas funcionales:**          <a name="funcionales"></a>

Las pruebas funcionales se enfocan en garantizar que se cubran los requerimientos funcionales de la aplicación, se escriben desde la perspectiva del usuario, con la intensión de confirmar que el aplicativo haga lo que el usuario espera obtener.

### Herramientas:

#### Selenium 

Esta herramienta automatiza el uso del navegador, como objetivo principal es la automatización de pruebas sobre aplicaciones web, las tareas del navegador tambien deben automatizarse.

La versión a utilizar de Selenium sera: 4.1


## Arquitectura a implementar <a name="arquitectura"></a>

![][diagrama]


La arquitectura a implementar sera totalmente funcional y basada en SOA y Microservicios.

SOA es una arquitectura orientada a servicios, es un tipo de diseño de software que permite reutilizar sus elementos por las interfaces de servicios que se comunican a través de una red con un lenguaje en común.

La arquitectura que se utilizara en el proyecto SoccerStats sera basado en servicios donde un servicio es una unidad autónoma de una o más funciones del software, un servicio esta diseñado para realizar una tarea específica, como recuperar cierta información o ejecutar una operación, cada servicio contiene las integraciones de datos y código que se necesitan para llevar a cabo una función especifica, completa y diferenciada. Para acceder a un servicio, se puede realizar de forma remota e interactuar con él o actualizarlo de manera independiente.

SockerStats integra los elementos del software que se implementaran y se mantendran por separado, se podra permitir que los servicios creados se comuniquen entre sí y trabajen en conjunto para formar la aplicacion de software en distintos sistemas.

Se a elegido la arquitectura SOA para el proyecto SoccerStats yaque nos proporciona muchos beneficios para la realización del proyecto, uno de los principales motivos es la reutilización de componenetes, lo cual permite aprovechar los servicios y disponer de ellos en cualquier y menor tiempo de respuesta.
otra ventaja es la reutilización la cual proporciona interoperabilidad entre tecnologías, y de esta forma tener cada servicio disponible para cualquier desarrollador.

Otros beneficios por la que se eligio la arquitectura de Microservicios:

1. Los microservicios aumentan la eficiencia en los procesos.
2. Amortiza la inversión realizada en el sistema.
3. Reduce costes de mantenimiento.
4. Facilita la adaptación al cambio, con la integración con sistemas heredados.
5. Fomenta la innovación orientada al desarrollo de servicios, acordes con el dinamismo de mercado.
6. Simplifica el diseño, optimizando la capacidad de organización.

Como aplicar la arquitectura SOA en SoccerStats:

1. Alinear todos los elementos, sistemas, procesos, informacion y aplicaciones en el negocio.
2. Aplicar conocimientos sobre microservicios y web services para estructurar el plan de negocio.
3. Aplicación de analisis de técnicas de diseño y desarrollo.
4. Al realizarse de forma grupal, se debe tener en cuenta la forma de trabajar y aplicar la arquitectura.

Desventajas al implementar SOA:

1. Falta de disponibilidad de servicios cuando aun no estan implementados.
2. Falta de recursos cuando se comparten equipos de desarrollo y tecnología.
3. Restricción de tiempo como variable asociada al proyecto.
4. Cambio del comportamiento del servicio, que podria invalidar los flujos de trabajo, e incide en la consistencia de información.

## Maquetacion <a name="maquetacion"></a>

### Inicio de la pagina web

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Inicio.png)

### Login

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Login.png)

### Registro de usuario

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Registro.png)

### Vista de administrador con reportes

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Administrador%20reportes.png)

### Vista de administrador con usuarios

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Administrador%20usuarios.png)

### Vista de empleado

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Empleado.png)

### Vista de cliente sin membresia

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Cliente%20sin%20membresia.png)

### Vista de cliente con membresia

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Cliente%20con%20membresia%20.png)

### Vista de login desde la aplicacion movil

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Movil%20Login.png)

### Vista de cliente desde la aplicacion movil

![alt text](https://gitlab.com/SelvinLP/SoccerStats/-/raw/feature/documentacion/Maquetacion/Movil%20Funcionalidades.png)


## Algoritmo de Prediccion

Para el algoritmo de prediccion se tomaran en cuenta entre 1 a 4 partidos previos. Para cada uno de los equipos a predecir se toma la cantidad de goles que anoto en los ultimos partidos y esta se divide dentro de la cantidad de partidos que se toman en cuenta, en este caso, 4. La idea es obtener un promedio de goles por equipo.

Para los equipos que juegan en casa, este valor se multiplica por 1.1 (esto implica que deberian de marcar un 10% mas de goles en promedio cuando juegan en casa), mientras que el promedio de los equipos visitantes se multiplica por un factor de 0.9 (esto implica que deberian de marcar un 10% menos de goles en promedio cuando juegan de visita).

A los resultados del proceso anterior se les aplica la formula de distribucion de Poisson para calcular la probabilidad de goles marcados en 90 minutos. Las distribuciones de Poisson son excelentes para predecir resultados discretos durante un período de tiempo fijo, por lo que son tan populares en el modelado deportivo.

![][poisson]

## Diagramas de diseño <a name="diseno"></a>
### Casos de Uso 
#### - Administrador:
![](Imagenes/CasoUso_Admin.PNG)
#### - Empleado:
![](Imagenes/CasoUso_Empleado.PNG)
#### - Cliente:
![](Imagenes/CasoUso_Cliente.PNG)

### Diagrama de Componentes
![](Imagenes/DiagramaComponentes.png)

### Diagrama de Despliegue
![](Imagenes/DiagramaDespliegue.png)

### Diagrama de Flujo DevOps
![](Imagenes/Diagrama_FlujoProcesoDevOps.PNG)

## Video solución de SoccerStats <a name="video"></a>

https://youtu.be/WLJ-zipI-cY


[diagrama]: Imagenes/diagrama_arquitectura.png
[poisson]: Imagenes/formula_dist_poisson.jpeg
