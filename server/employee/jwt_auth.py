from functools import wraps
from flask import jsonify, request
import psycopg2
import jwt

#Autenticacion JWT
def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']
        if not token:
            return jsonify({'status': -1, 'message': 'Token valido faltante.'})
        try:
            data = jwt.decode(token, "004f2af45d3a4e161a7dd2d17fdae47f", algorithms = ["HS256"])

            #Conexion con base de datos PostgreSQL
            connection = psycopg2.connect(
                user        = "admin_ge",
                password    = "sa-ge",
                host        = "18.189.78.76",
                port        = "5432",
                database    = "soccerstats"
            )
            cursor = connection.cursor()

            #Consulta para obtener los datos del usuario
            postgreSQL_get_user_query = "SELECT * FROM User_ WHERE User_.user_id = %s"
            cursor.execute(postgreSQL_get_user_query, (data["user_id"],))
            current_user = cursor.fetchone()
            
        except Exception as error:
            return jsonify({'status': -1, 'message': 'Token invalido.', 'error': str(error)})
        return f(current_user, *args, **kwargs)
    return decorator