from flask import Flask, jsonify, request
from flask_socketio import SocketIO
from flask_cors import CORS
import psycopg2
import datetime


app = Flask(__name__)
CORS(app)
socket = SocketIO(app, cors_allowed_origins="*")


@app.route('/esb/team/init', methods = ['GET'])
def init():
    return "Microservicios equipo trabajando correctamente."

#########################################
###            CRUD EQUIPO            ###
#########################################

#Crear equipo
@app.route('/esb/team', methods = ['POST'])
def createTeam():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar equipo
        insert_query = "INSERT INTO Team(team_name, team_foundation_date, team_photo, team_id_country) VALUES (%s, %s, %s, %s)"
        insert_query_values = (data["name"], data["foundation_date"], data["photo"], data["id_country"])
        cursor.execute(insert_query, insert_query_values)

        #Commit para permanencia de datos y cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Equipo creado con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al crear equipo. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Ver equipo(s)
@app.route('/esb/team', methods = ['GET'])
def getTeams():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener equipo(s)
        get_team_query = """SELECT t1.team_id, t1.team_name, t1.team_foundation_date, t1.team_photo, c1.country_id, c1.country_name
            FROM Team AS t1 INNER JOIN Country AS c1 ON c1.country_id = t1.team_id_country"""

        #Verificar si viene el parametro 'id'
        team_id = request.args.get('id')
        if team_id != None:
            get_team_query = get_team_query + " WHERE t1.team_id = %s"
            cursor.execute(get_team_query, (team_id,))
        else:
            cursor.execute(get_team_query)

        #Retorno de equipos
        teams_data = cursor.fetchall()
        for team_data_ in teams_data:
            res["data"].append({
                "id"                : team_data_[0],
                "name"              : team_data_[1],
                "foundation_date"   : team_data_[2],
                "photo"             : team_data_[3],
                "id_country"        : team_data_[4],
                "country"           : team_data_[5]
            })
        
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Equipo(s) obtenido(s) con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener equipo(s). (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Modificar equipo
@app.route('/esb/team', methods = ['PUT'])
def updateTeam():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el equipo
        verify_query = "SELECT COUNT(*) FROM Team WHERE team_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para actualizar equipo
            update_query = "UPDATE Team SET team_name = %s, team_foundation_date = %s, team_photo = %s, team_id_country = %s WHERE team_id = %s"
            update_query_values = (data["name"], data["foundation_date"], data["photo"], data["id_country"], data["id"])
            cursor.execute(update_query, update_query_values)

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Equipo actualizado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar equipo."
            res["data"].append("No existe ningún equipo con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar equipo. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Eliminar equipo
@app.route('/esb/team', methods = ['DELETE'])
def deleteTeam():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el equipo
        verify_query = "SELECT COUNT(*) FROM Team WHERE team_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para eliminar equipo
            postgreSQL_delete_query = "DELETE FROM Team WHERE team_id = %s"
            cursor.execute(postgreSQL_delete_query, (data["id"],))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Equipo eliminado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al eliminar equipo."
            res["data"].append("No existe ningún equipo con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al eliminar equipo. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)


#########################################
###            CRUD ESTADIO           ###
#########################################

#Crear estadio
@app.route('/esb/stadium', methods = ['POST'])
def createStadium():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar estadio
        insert_query = """INSERT INTO Stadium(stadium_name, stadium_foundation_date, stadium_capacity, stadium_address, stadium_photo, stadium_status, stadium_id_country)
        VALUES (%s, %s, %s, %s, %s, %s, %s)"""  
        insert_query_values = (data["name"], data["foundation_date"], data["capacity"], data["address"], data["photo"], data["status"], data["id_country"])
        cursor.execute(insert_query, insert_query_values)

        #Commit para permanencia de datos y cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Estadio creado con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al crear estadio. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Ver estadio(s)
@app.route('/esb/stadium', methods = ['GET'])
def getStaduims():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener estadio(s)
        get_stadium_query = """SELECT s1.stadium_id, s1.stadium_name, s1.stadium_foundation_date, s1.stadium_capacity,
            c1.country_id, c1.country_name, s1.stadium_address, s1.stadium_status, s1.stadium_photo
            FROM Stadium AS s1 INNER JOIN Country AS c1 ON c1.country_id = s1.stadium_id_country"""

        #Verificar si viene el parametro 'id'
        stadium_id = request.args.get('id')
        if stadium_id != None:
            get_stadium_query = get_stadium_query + " WHERE s1.stadium_id = %s"
            cursor.execute(get_stadium_query, (stadium_id,))
        else:
            cursor.execute(get_stadium_query)

        #Retorno de estadios
        stadiums_data = cursor.fetchall()
        for stadium_data_ in stadiums_data:
            res["data"].append({
                "id"                : stadium_data_[0],
                "name"              : stadium_data_[1],
                "foundation_date"   : stadium_data_[2],
                "capacity"          : stadium_data_[3],
                "id_country"        : stadium_data_[4],
                "country"           : stadium_data_[5],
                "address"           : stadium_data_[6],
                "status"            : stadium_data_[7],
                "photo"             : stadium_data_[8]
            })
        
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Estadio(s) obtenido(s) con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener estadio(s). (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Modificar estadio
@app.route('/esb/stadium', methods = ['PUT'])
def updateStadium():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el estadio
        verify_query = "SELECT COUNT(*) FROM Stadium WHERE stadium_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para actualizar estadio
            update_query = """UPDATE Stadium SET stadium_name = %s, stadium_foundation_date = %s, stadium_capacity = %s,
                stadium_address = %s, stadium_photo = %s, stadium_status = %s, stadium_id_country = %s WHERE stadium_id = %s"""
            query_values = (data["name"], data["foundation_date"], data["capacity"], data["address"], data["photo"], data["status"], data["id_country"], data["id"])
            cursor.execute(update_query, query_values)

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Estadio actualizado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar estadio."
            res["data"].append("No existe ningún estadio con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar estadio. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Eliminar estadio
@app.route('/esb/stadium', methods = ['DELETE'])
def deleteStadium():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el estadio
        verify_query = "SELECT COUNT(*) FROM Stadium WHERE stadium_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para eliminar estadio
            postgreSQL_delete_query = "DELETE FROM Stadium WHERE stadium_id = %s"
            cursor.execute(postgreSQL_delete_query, (data["id"],))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Estadio eliminado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al eliminar estadio."
            res["data"].append("No existe ningún estadio con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al eliminar estadio. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#########################################
###              CRUD DT              ###
#########################################

#Crear DT
@app.route('/esb/technical-director', methods = ['POST'])
def createDT():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar DT
        user_age = datetime.datetime.utcnow().year - datetime.datetime.strptime(data["birth_date"], "%Y-%m-%d").year
        insert_query = """INSERT INTO DT(dt_name, dt_lastname, dt_birth_date, dt_photo, dt_status, dt_id_team, dt_id_country, dt_age) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"""  
        insert_query_values = (data["name"], data["lastname"], data["birth_date"], data["photo"], data["status"], str(1), data["id_country"], user_age)
        cursor.execute(insert_query, insert_query_values)

        #Commit para permanencia de datos y cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Directo técnico creado con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al crear directo técnico. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Ver DT(s)
@app.route('/esb/technical-director', methods = ['GET'])
def getDTs():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener DT(s) con equipo y sin equipo
        get_dt_query = """SELECT d1.dt_id, d1.dt_name, d1.dt_lastname, d1.dt_birth_date,
            d1.dt_status,d1.dt_photo, t1.team_id, t1.team_name, c1.country_id, c1.country_name
            FROM DT AS d1
                INNER JOIN Country AS c1 ON c1.country_id = d1.dt_id
                INNER JOIN Team AS t1 ON t1.team_id = d1.dt_id_team"""

        #Verificar si viene el parametro 'id'
        dt_id = request.args.get('id')
        if dt_id != None:
            get_dt_query = get_dt_query + " WHERE d1.dt_id = %s"
            cursor.execute(get_dt_query, (dt_id,))
        else:
            cursor.execute(get_dt_query)

        #Retorno de DTs
        dts_data = cursor.fetchall()
        for dt_data_ in dts_data:
            res["data"].append({
                "id"            : dt_data_[0],
                "name"          : dt_data_[1],
                "lastname"      : dt_data_[2],
                "birth_date"    : dt_data_[3],
                "status"        : dt_data_[4],
                "photo"         : dt_data_[5],
                "id_team"       : dt_data_[6],
                "name_team"     : dt_data_[7],
                "id_country"    : dt_data_[8],
                "country"       : dt_data_[9]
            })
        
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "DT(s) obtenido(s) con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener DT(s). (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Modificar DT
@app.route('/esb/technical-director', methods = ['PUT'])
def updateDT():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el DT
        verify_query = "SELECT COUNT(*) FROM DT WHERE dt_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para actualizar DT
            update_query = """UPDATE DT SET dt_name = %s, dt_lastname = %s, dt_birth_date = %s, dt_status = %s, dt_photo = %s, dt_id_country = %s WHERE dt_id = %s"""
            query_values = (data["name"], data["lastname"], data["birth_date"], data["status"], data["photo"], data["id_country"], data["id"])
            cursor.execute(update_query, query_values)

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "DT actualizado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar DT."
            res["data"].append("No existe ningún DT con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar DT. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Eliminar DT
@app.route('/esb/technical-director', methods = ['DELETE'])
def deleteDT():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el DT
        verify_query = "SELECT COUNT(*) FROM DT WHERE dt_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para eliminar DT
            postgreSQL_delete_query = "DELETE FROM DT WHERE dt_id = %s"
            cursor.execute(postgreSQL_delete_query, (data["id"],))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "DT eliminado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al eliminar DT."
            res["data"].append("No existe ningún DT con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al eliminar DT. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#########################################
###            CRUD Jugador           ###
#########################################

#Crear Jugador
@app.route('/esb/player', methods = ['POST'])
def createPlayer():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar Jugador
        user_age = datetime.datetime.utcnow().year - datetime.datetime.strptime(data["birth_date"], "%Y-%m-%d").year
        insert_query = """INSERT INTO Player(player_name, player_lastname, player_birth_date, player_id_nationality, player_position,
            player_status, player_photo, player_id_team, player_age) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""  
        insert_query_values = (data["name"], data["lastname"], data["birth_date"], data["id_nationality"], data["position"], data["status"], data["photo"], str(1), user_age)
        cursor.execute(insert_query, insert_query_values)

        #Commit para permanencia de datos y cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Jugador creado con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al crear jugador. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Ver Jugador(es)
@app.route('/esb/player', methods = ['GET'])
def getPlayers():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener Jugador(es) con equipo y sin equipo
        get_player_query = """SELECT d1.player_id, d1.player_name, d1.player_lastname, d1.player_birth_date,
            c1.country_id, c1.country_name, d1.player_position, d1.player_status, t1.team_id, t1.team_name, d1.player_photo
            FROM Player AS d1
                INNER JOIN Country AS c1 ON c1.country_id = d1.player_id_nationality
                INNER JOIN Team AS t1 ON t1.team_id = d1.player_id_team"""

        #Verificar si viene el parametro 'id'
        player_id = request.args.get('id')
        if player_id != None:
            get_player_query = get_player_query + " WHERE d1.player_id = %s"
            cursor.execute(get_player_query, (player_id,))
        else:
            cursor.execute(get_player_query)

        #Retorno de Jugadores
        players_data = cursor.fetchall()
        for player_data_ in players_data:
            res["data"].append({
                "id"                : player_data_[0],
                "name"              : player_data_[1],
                "lastname"          : player_data_[2],
                "birth_date"        : player_data_[3],
                "id_nationality"    : player_data_[4],
                "nationality"       : player_data_[5],
                "position"          : player_data_[6],
                "status"            : player_data_[7],
                "id_team"           : player_data_[8],
                "name_team"         : player_data_[9],
                "photo"             : player_data_[10],
            })
        
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Jugador(es) obtenido(s) con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener Jugador(es). (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Modificar Jugador
@app.route('/esb/player', methods = ['PUT'])
def updatePlayer():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el Jugador
        verify_query = "SELECT COUNT(*) FROM Player WHERE player_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para actualizar Jugador
            update_query = """UPDATE Player SET player_name = %s, player_lastname = %s, player_birth_date = %s, player_id_nationality = %s,
                player_position = %s, player_status = %s, player_photo = %s WHERE player_id = %s"""
            query_values = (data["name"], data["lastname"], data["birth_date"], data["id_nationality"], data["position"], data["status"], data["photo"], data["id"])
            cursor.execute(update_query, query_values)

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Jugador actualizado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar jugador."
            res["data"].append("No existe ningún jugador con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar jugador. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Eliminar Jugador
@app.route('/esb/player', methods = ['DELETE'])
def deletePlayer():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el jugador
        verify_query = "SELECT COUNT(*) FROM Player WHERE player_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para eliminar jugador
            postgreSQL_delete_query = "DELETE FROM Player WHERE player_id = %s"
            cursor.execute(postgreSQL_delete_query, (data["id"],))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Jugador eliminado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al eliminar jugador."
            res["data"].append("No existe ningún jugador con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al eliminar jugador. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#########################################
###         CRUD Competencia          ###
#########################################

#Crear Competencia
@app.route('/esb/competition', methods = ['POST'])
def createCompetition():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar Competencia
        insert_query = """INSERT INTO Competition(competition_name, competition_type, competition_year, competition_champion_id, competition_country_id) VALUES (%s, %s, %s, %s, %s)"""  
        insert_query_values = (data["name"], data["type"], data["year"], str(1), data["id_country"])
        cursor.execute(insert_query, insert_query_values)

        #Commit para permanencia de datos y cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Competencia creada con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al crear competencia. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Ver Competencia(s)
@app.route('/esb/competition', methods = ['GET'])
def getCompetitions():  
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener Competencia(s)
        get_competition_query = """SELECT d1.competition_id, d1.competition_name, d1.competition_type,
            d1.competition_year, t1.team_id, t1.team_name, c1.country_id, c1.country_name
            FROM Competition AS d1
                INNER JOIN Country AS c1 ON c1.country_id = d1.competition_country_id
                INNER JOIN Team AS t1 ON t1.team_id = d1.competition_champion_id"""

        #Verificar si viene el parametro 'id'
        competition_id = request.args.get('id')
        if competition_id != None:
            get_competition_query = get_competition_query + " WHERE d1.competition_id = %s"
            cursor.execute(get_competition_query, (competition_id,))
        else:
            cursor.execute(get_competition_query)

        #Retorno de Competencias
        competition_data = cursor.fetchall()
        for competition_data_ in competition_data:
            res["data"].append({
                "id"                : competition_data_[0],
                "name"              : competition_data_[1],
                "type"              : competition_data_[2],
                "year"              : competition_data_[3],
                "id_champion_team"  : competition_data_[4],
                "champion_team"     : competition_data_[5],
                "id_country"        : competition_data_[6],
                "country"           : competition_data_[7]
            })
        
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Competencia(s) obtenida(s) con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener competencia(s). (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Modificar Competencia
@app.route('/esb/competition', methods = ['PUT'])
def updateCompetition():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista la competencia
        verify_query = "SELECT COUNT(*) FROM Competition WHERE competition_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para actualizar competencia
            update_query = """UPDATE Competition SET competition_name = %s, competition_type = %s, competition_year = %s,
                competition_champion_id = %s, competition_country_id = %s WHERE competition_id = %s"""
            query_values = (data["name"], data["type"], data["year"], data["id_champion_team"], data["id_country"], data["id"])
            cursor.execute(update_query, query_values)

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Competencia actualizada con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar competencia."
            res["data"].append("No existe ningúna competencia con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar jugador. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Eliminar Competencia
@app.route('/esb/competition', methods = ['DELETE'])
def deleteCompetition():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista la competencia
        verify_query = "SELECT COUNT(*) FROM Competition WHERE competition_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para eliminar competencia
            postgreSQL_delete_query = "DELETE FROM Competition WHERE competition_id = %s"
            cursor.execute(postgreSQL_delete_query, (data["id"],))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Competencia eliminada con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al eliminar competencia."
            res["data"].append("No existe ningúna competencia con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al eliminar jugador. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#########################################
###           CRUD Partido            ###
#########################################

#Crear Partido
@app.route('/esb/match', methods = ['POST'])
def createMatch():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar partido
        insert_query = """INSERT INTO Match_(match_game_date, match_attendees, match_result_local, match_result_visiting, match_status,
            match_id_stadium, match_id_team_local, match_id_team_visiting, match_id_competition) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""  
        insert_query_values = (data["game_date"], data["attendees"], data["result_local"], data["result_visiting"], data["status"],
            data["id_stadium"], data["id_team_local"], data["id_team_visiting"], data["id_competition"])
        cursor.execute(insert_query, insert_query_values)

        #Commit para permanencia de datos y cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Partido creado con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al crear partido. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Ver Partido(s)
@app.route('/esb/match', methods = ['GET'])
def getMatches():  
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener partido(s)
        get_match_query = """SELECT d1.match_id, d1.match_game_date, d1.match_attendees, d1.match_result_local, d1.match_result_visiting, d1.match_status,
            s1.stadium_id, s1.stadium_name, t1.team_id, t1.team_name, t2.team_id, t2.team_name, c1.competition_id, c1.competition_name
            FROM Match_ AS d1
                INNER JOIN Stadium AS s1 ON s1.stadium_id = d1.match_id_stadium
                INNER JOIN Team AS t1 ON t1.team_id = d1.match_id_team_local
                INNER JOIN Team AS t2 ON t2.team_id = d1.match_id_team_visiting
                INNER JOIN Competition AS c1 ON c1.competition_id = d1.match_id_competition"""

        #Verificar si viene el parametro 'id'
        match_id = request.args.get('id')
        if match_id != None:
            get_match_query = get_match_query + " WHERE d1.match_id = %s"
            cursor.execute(get_match_query, (match_id,))
        else:
            cursor.execute(get_match_query)

        #Retorno de partidos
        match_data = cursor.fetchall()
        for match_data_ in match_data:
            res["data"].append({
                "id"                : match_data_[0],
                "game_date"         : match_data_[1],
                "attendees"         : match_data_[2],
                "result_local"      : match_data_[3],
                "result_visiting"   : match_data_[4],
                "status"            : match_data_[5],
                "id_stadium"        : match_data_[6],
                "stadium"           : match_data_[7],
                "id_team_local"     : match_data_[8],
                "team_local"        : match_data_[9],
                "id_team_visiting"  : match_data_[10],
                "team_visiting"     : match_data_[11],
                "id_competition"    : match_data_[12],
                "competition"       : match_data_[13],
            })
        
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "Partido(s) obtenido(s) con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener partido(s). (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Modificar Partido
@app.route('/esb/match', methods = ['PUT'])
def updateMatch():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el partido
        verify_query = "SELECT COUNT(*) FROM Match_ WHERE match_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para actualizar partido
            update_query = """UPDATE Match_ SET match_game_date = %s, match_attendees = %s, match_result_local = %s, match_result_visiting = %s, match_status = %s,
                match_id_stadium = %s, match_id_team_local = %s, match_id_team_visiting = %s, match_id_competition = %s WHERE match_id = %s"""
            query_values = (data["game_date"], data["attendees"], data["result_local"], data["result_visiting"], data["status"],
                data["id_stadium"], data["id_team_local"], data["id_team_visiting"], data["id_competition"], data["id"])
            cursor.execute(update_query, query_values)

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Partido actualizado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar partido."
            res["data"].append("No existe ningún partido con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar partido. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Eliminar Partido
@app.route('/esb/match', methods = ['DELETE'])
def deleteMatch():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el partido
        verify_query = "SELECT COUNT(*) FROM Match_ WHERE match_id = %s"
        cursor.execute(verify_query, (data["id"],))
        if cursor.fetchone()[0] == 1:

            #Consulta para eliminar partido
            postgreSQL_delete_query = "DELETE FROM Match_ WHERE match_id = %s"
            cursor.execute(postgreSQL_delete_query, (data["id"],))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Partido eliminado con éxito."

        else:
            res["status"] = 400
            res["msg"] = "Error al eliminar partido."
            res["data"].append("No existe ningún partido con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al eliminar jugador. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#########################################
###        Acciones Empleado          ###
#########################################

#Transferir Jugador
@app.route('/esb/employee/player-transfer', methods = ['POST'])
def transferPlayer():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para transferir jugador a un equipo
        transfer_query = "UPDATE Player SET player_id_team = %s WHERE player_id = %s"
        cursor.execute(transfer_query, (data["id_team_destination"], data["id_player"]))

        log_query = "INSERT INTO Log_Player(start_date, end_date, log_team_id, log_player_id) VALUES (%s, %s, %s, %s)"
        cursor.execute(log_query, (data["start_date"], data["end_date"], data["id_team_origin"], data["id_player"]))

        #Commit para permanencia de datos
        connection.commit()
        res["status"] = 200
        res["msg"] = "Jugador transferido con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al transferir jugador. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Transferir DT
@app.route('/esb/employee/technical-director-transfer', methods = ['POST'])
def transferDT():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para transferir DT a un equipo
        transfer_query = "UPDATE DT SET dt_id_team = %s WHERE dt_id = %s"
        cursor.execute(transfer_query, (data["id_team_destination"], data["id_coach"]))

        log_query = "INSERT INTO Log_DT(start_date, end_date, log_team_id, log_dt_id) VALUES (%s, %s, %s, %s)"
        cursor.execute(log_query, (data["start_date"], data["end_date"], data["id_team_origin"], data["id_coach"]))

        #Commit para permanencia de datos
        connection.commit()
        res["status"] = 200
        res["msg"] = "Director técnico transferido con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al transferir director técnico. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Agregar Incidencia
@app.route('/esb/employee/incidence', methods = ['POST'])
def createIncidence():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar incidencia de un partido
        insert_query = "INSERT INTO Incidence(incidence_type, incidence_description, incidence_minute, incidence_id_player, incidence_id_game) VALUES(%s, %s, %s, %s, %s)"
        cursor.execute(insert_query, (data["id_type"], data["description"], data["minute"], data["id_player"], data["id_game"]))

        #Commit para permanencia de datos
        connection.commit()
        res["status"] = 200
        res["msg"] = "Incidencia agregada con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al agregar incidencia. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Crear Noticia
@app.route('/esb/post', methods = ['POST'])
def createPost():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para insertar noticio de un equipo
        insert_query = "INSERT INTO Notice_(notice_title, notice_description, notice_date, notice_id_team, notice_id_user) VALUES(%s, %s, %s, %s, %s)"
        cursor.execute(insert_query, (data["title"], data["description"], data["date"], data["id_team"], data["id_user"]))

        #Commit para permanencia de datos
        connection.commit()
        res["status"] = 200
        res["msg"] = "Noticia creada con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al crear noticia. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5012, debug = True)
