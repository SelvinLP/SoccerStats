from flask import Flask, jsonify, request
from flask_socketio import SocketIO
from flask_cors import CORS
import psycopg2
import smtplib
from email.mime.text import MIMEText
from functools import wraps
import datetime
import jwt
from uuid import uuid4
import hashlib


app = Flask(__name__)
CORS(app)
socket = SocketIO(app, cors_allowed_origins="*")
app.config['SECRET_KEY'] = 'SiSaleSA_'



dropdownCountries = [
    { 'code': 1, 'name':'Afghanistan'},
    { 'code': 2, 'name':'Albania'},
    { 'code': 3, 'name':'Algeria'},
    { 'code': 4, 'name':'American Samoa'},
    { 'code': 5, 'name':'Andorra'},
    { 'code': 6, 'name':'Angola'},
    { 'code': 7, 'name':'Anguilla'},
    { 'code': 8, 'name':'Antarctica'},
    { 'code': 9, 'name':'Antigua and Barbuda'},
    { 'code': 10, 'name':'Argentina'},
    { 'code': 11, 'name':'Armenia'},
    { 'code': 12, 'name':'Aruba'},
    { 'code': 13, 'name':'Australia'},
    { 'code': 14, 'name':'Austria'},
    { 'code': 15, 'name':'Azerbaijan'},
    { 'code': 16, 'name':'Bahamas'},
    { 'code': 17, 'name':'Bahrain'},
    { 'code': 18, 'name':'Bangladesh'},
    { 'code': 19, 'name':'Barbados'},
    { 'code': 20, 'name':'Belarus'},
    { 'code': 21, 'name':'Belgium'},
    { 'code': 22, 'name':'Belize'},
    { 'code': 23, 'name':'Benin'},
    { 'code': 24, 'name':'Bermuda'},
    { 'code': 25, 'name':'Bhutan'},
    { 'code': 26, 'name':'Bolivia'},
    { 'code': 27, 'name':'Bosnia and Herzegovina'},
    { 'code': 28, 'name':'Botswana'},
    { 'code': 29, 'name':'Bouvet Island'},
    { 'code': 30, 'name':'Brazil'},
    { 'code': 31, 'name':'British Indian Ocean Territory'},
    { 'code': 32, 'name':'Brunei Darussalam'},
    { 'code': 33, 'name':'Bulgaria'},
    { 'code': 34, 'name':'Burkina Faso'},
    { 'code': 35, 'name':'Burundi'},
    { 'code': 36, 'name':'Cambodia'},
    { 'code': 37, 'name':'Cameroon'},
    { 'code': 38, 'name':'Canada'},
    { 'code': 39, 'name':'Cape Verde'},
    { 'code': 40, 'name':'Cayman Islands'},
    { 'code': 41, 'name':'Central African Republic'},
    { 'code': 42, 'name':'Chad'},
    { 'code': 43, 'name':'Chile'},
    { 'code': 44, 'name':'China'},
    { 'code': 45, 'name':'Christmas Island'},
    { 'code': 46, 'name':'Cocos { code: Keeling} Islands'},
    { 'code': 47, 'name':'Colombia'},
    { 'code': 48, 'name':'Comoros'},
    { 'code': 49, 'name':'Congo'},
    { 'code': 50, 'name':'Congo, name:the Democratic Republic of the'},
    { 'code': 51, 'name':'Cook Islands'},
    { 'code': 52, 'name':'Costa Rica'},
    { 'code': 53, 'name':'Cote DIvoire'},
    { 'code': 54, 'name':'Croatia'},
    { 'code': 55, 'name':'Cuba'},
    { 'code': 56, 'name':'Cyprus'},
    { 'code': 57, 'name':'Czech Republic'},
    { 'code': 58, 'name':'Denmark'},
    { 'code': 59, 'name':'Djibouti'},
    { 'code': 60, 'name':'Dominica'},
    { 'code': 61, 'name':'Dominican Republic'},
    { 'code': 62, 'name':'Ecuador'},
    { 'code': 63, 'name':'Egypt'},
    { 'code': 64, 'name':'El Salvador'},
    { 'code': 65, 'name':'Equatorial Guinea'},
    { 'code': 66, 'name':'Eritrea'},
    { 'code': 67, 'name':'Estonia'},
    { 'code': 68, 'name':'Ethiopia'},
    { 'code': 69, 'name':'Falkland Islands { code: Malvinas}'},
    { 'code': 70, 'name':'Faroe Islands'},
    { 'code': 71, 'name':'Fiji'},
    { 'code': 72, 'name':'Finland'},
    { 'code': 73, 'name':'France'},
    { 'code': 74, 'name':'French Guiana'},
    { 'code': 75, 'name':'French Polynesia'},
    { 'code': 76, 'name':'French Southern Territories'},
    { 'code': 77, 'name':'Gabon'},
    { 'code': 78, 'name':'Gambia'},
    { 'code': 79, 'name':'Georgia'},
    { 'code': 80, 'name':'Germany'},
    { 'code': 81, 'name':'Ghana'},
    { 'code': 82, 'name':'Gibraltar'},
    { 'code': 83, 'name':'Greece'},
    { 'code': 84, 'name':'Greenland'},
    { 'code': 85, 'name':'Grenada'},
    { 'code': 86, 'name':'Guadeloupe'},
    { 'code': 87, 'name':'Guam'},
    { 'code': 88, 'name':'Guatemala'},
    { 'code': 89, 'name':'Guinea'},
    { 'code': 90, 'name':'Guinea-Bissau'},
    { 'code': 91, 'name':'Guyana'},
    { 'code': 92, 'name':'Haiti'},
    { 'code': 93, 'name':'Heard Island and Mcdonald Islands'},
    { 'code': 94, 'name':'Holy See { code: Vatican City State}'},
    { 'code': 95, 'name':'Honduras'},
    { 'code': 96, 'name':'Hong Kong'},
    { 'code': 97, 'name':'Hungary'},
    { 'code': 98, 'name':'Iceland'},
    { 'code': 99, 'name':'India'},
    { 'code': 100, 'name':'Indonesia'},
    { 'code': 101, 'name':'Iran, name:Islamic Republic of'},
    { 'code': 102, 'name':'Iraq'},
    { 'code': 103, 'name':'Ireland'},
    { 'code': 104, 'name':'Israel'},
    { 'code': 105, 'name':'Italy'},
    { 'code': 106, 'name':'Jamaica'},
    { 'code': 107, 'name':'Japan'},
    { 'code': 108, 'name':'Jordan'},
    { 'code': 109, 'name':'Kazakhstan'},
    { 'code': 110, 'name':'Kenya'},
    { 'code': 111, 'name':'Kiribati'},
    { 'code': 112, 'name':'Korea, name:Democratic Peoples Republic of'},
    { 'code': 113, 'name':'Korea, name:Republic of'},
    { 'code': 114, 'name':'Kuwait'},
    { 'code': 115, 'name':'Kyrgyzstan'},
    { 'code': 116, 'name':'Lao Peoples Democratic Republic'},
    { 'code': 117, 'name':'Latvia'},
    { 'code': 118, 'name':'Lebanon'},
    { 'code': 119, 'name':'Lesotho'},
    { 'code': 120, 'name':'Liberia'},
    { 'code': 121, 'name':'Libyan Arab Jamahiriya'},
    { 'code': 122, 'name':'Liechtenstein'},
    { 'code': 123, 'name':'Lithuania'},
    { 'code': 124, 'name':'Luxembourg'},
    { 'code': 125, 'name':'Macao'},
    { 'code': 126, 'name':'Macedonia, name:the Former Yugoslav Republic of'},
    { 'code': 127, 'name':'Madagascar'},
    { 'code': 128, 'name':'Malawi'},
    { 'code': 129, 'name':'Malaysia'},
    { 'code': 130, 'name':'Maldives'},
    { 'code': 131, 'name':'Mali'},
    { 'code': 132, 'name':'Malta'},
    { 'code': 133, 'name':'Marshall Islands'},
    { 'code': 134, 'name':'Martinique'},
    { 'code': 135, 'name':'Mauritania'},
    { 'code': 136, 'name':'Mauritius'},
    { 'code': 137, 'name':'Mayotte'},
    { 'code': 138, 'name':'Mexico'},
    { 'code': 139, 'name':'Micronesia, name:Federated States of'},
    { 'code': 140, 'name':'Moldova, name:Republic of'},
    { 'code': 141, 'name':'Monaco'},
    { 'code': 142, 'name':'Mongolia'},
    { 'code': 143, 'name':'Montserrat'},
    { 'code': 144, 'name':'Morocco'},
    { 'code': 145, 'name':'Mozambique'},
    { 'code': 146, 'name':'Myanmar'},
    { 'code': 147, 'name':'Namibia'},
    { 'code': 148, 'name':'Nauru'},
    { 'code': 149, 'name':'Nepal'},
    { 'code': 150, 'name':'Netherlands'},
    { 'code': 151, 'name':'Netherlands Antilles'},
    { 'code': 152, 'name':'New Caledonia'},
    { 'code': 153, 'name':'New Zealand'},
    { 'code': 154, 'name':'Nicaragua'},
    { 'code': 155, 'name':'Niger'},
    { 'code': 156, 'name':'Nigeria'},
    { 'code': 157, 'name':'Niue'},
    { 'code': 158, 'name':'Norfolk Island'},
    { 'code': 159, 'name':'Northern Mariana Islands'},
    { 'code': 160, 'name':'Norway'},
    { 'code': 161, 'name':'Oman'},
    { 'code': 162, 'name':'Pakistan'},
    { 'code': 163, 'name':'Palau'},
    { 'code': 164, 'name':'Palestinian Territory, name:Occupied'},
    { 'code': 165, 'name':'Panama'},
    { 'code': 166, 'name':'Papua New Guinea'},
    { 'code': 167, 'name':'Paraguay'},
    { 'code': 168, 'name':'Peru'},
    { 'code': 169, 'name':'Philippines'},
    { 'code': 170, 'name':'Pitcairn'},
    { 'code': 171, 'name':'Poland'},
    { 'code': 172, 'name':'Portugal'},
    { 'code': 173, 'name':'Puerto Rico'},
    { 'code': 174, 'name':'Qatar'},
    { 'code': 175, 'name':'Reunion'},
    { 'code': 176, 'name':'Romania'},
    { 'code': 177, 'name':'Russian Federation'},
    { 'code': 178, 'name':'Rwanda'},
    { 'code': 179, 'name':'Saint Helena'},
    { 'code': 180, 'name':'Saint Kitts and Nevis'},
    { 'code': 181, 'name':'Saint Lucia'},
    { 'code': 182, 'name':'Saint Pierre and Miquelon'},
    { 'code': 183, 'name':'Saint Vincent and the Grenadines'},
    { 'code': 184, 'name':'Samoa'},
    { 'code': 185, 'name':'San Marino'},
    { 'code': 186, 'name':'Sao Tome and Principe'},
    { 'code': 187, 'name':'Saudi Arabia'},
    { 'code': 188, 'name':'Senegal'},
    { 'code': 189, 'name':'Serbia and Montenegro'},
    { 'code': 190, 'name':'Seychelles'},
    { 'code': 191, 'name':'Sierra Leone'},
    { 'code': 192, 'name':'Singapore'},
    { 'code': 193, 'name':'Slovakia'},
    { 'code': 194, 'name':'Slovenia'},
    { 'code': 195, 'name':'Solomon Islands'},
    { 'code': 196, 'name':'Somalia'},
    { 'code': 197, 'name':'South Africa'},
    { 'code': 198, 'name':'South Georgia and the South Sandwich Islands'},
    { 'code': 199, 'name':'Spain'},
    { 'code': 200, 'name':'Sri Lanka'},
    { 'code': 201, 'name':'Sudan'},
    { 'code': 202, 'name':'Suriname'},
    { 'code': 203, 'name':'Svalbard and Jan Mayen'},
    { 'code': 204, 'name':'Swaziland'},
    { 'code': 205, 'name':'Sweden'},
    { 'code': 206, 'name':'Switzerland'},
    { 'code': 207, 'name':'Syrian Arab Republic'},
    { 'code': 208, 'name':'Taiwan, name:Province of China'},
    { 'code': 209, 'name':'Tajikistan'},
    { 'code': 210, 'name':'Tanzania, name:United Republic of'},
    { 'code': 211, 'name':'Thailand'},
    { 'code': 212, 'name':'Timor-Leste'},
    { 'code': 213, 'name':'Togo'},
    { 'code': 214, 'name':'Tokelau'},
    { 'code': 215, 'name':'Tonga'},
    { 'code': 216, 'name':'Trinidad and Tobago'},
    { 'code': 217, 'name':'Tunisia'},
    { 'code': 218, 'name':'Turkey'},
    { 'code': 219, 'name':'Turkmenistan'},
    { 'code': 220, 'name':'Turks and Caicos Islands'},
    { 'code': 221, 'name':'Tuvalu'},
    { 'code': 222, 'name':'Uganda'},
    { 'code': 223, 'name':'Ukraine'},
    { 'code': 224, 'name':'United Arab Emirates'},
    { 'code': 225, 'name':'United Kingdom'},
    { 'code': 226, 'name':'United States'},
    { 'code': 227, 'name':'United States Minor Outlying Islands'},
    { 'code': 228, 'name':'Uruguay'},
    { 'code': 229, 'name':'Uzbekistan'},
    { 'code': 230, 'name':'Vanuatu'},
    { 'code': 231, 'name':'Venezuela'},
    { 'code': 232, 'name':'Viet Nam'},
    { 'code': 233, 'name':'Virgin Islands, name:British'},
    { 'code': 234, 'name':'Virgin Islands, name:U.s.'},
    { 'code': 235, 'name':'Wallis and Futuna'},
    { 'code': 236, 'name':'Western Sahara'},
    { 'code': 237, 'name':'Yemen'},
    { 'code': 238, 'name':'Zambia'},
    { 'code': 239, 'name':'Zimbabwe'}
   ]

@app.route('/esb/user/client/create', methods = ['GET'])
def init():
    return "User microservice trabajando correctamente."

#Crear Usuario
@app.route('/esb/customer/register', methods = ['POST'])
def createUser():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que no existe el usuario
        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_email = %s"
        cursor.execute(find_user_query, (data["email"],))
        if cursor.fetchone()[0] == 0:
            # ROL
            # Administrador = 1
            # Empleado = 2
            # Cliente = 3

            #ESTADO DE CUENTA
            # Activa = 1 (Hasta verificacion)
            # Congelada = 2 (Estado inicial)
            # Eliminada = 3

            # has_membership:
            # 1: Si
            # 0: No

            
            #fechacreacion = datetime.today().strftime('%Y-%m-%d')
            membresia = 0
            userRol = 3
            status = 2

            #today = datetime.today()
            #Bday = data["birth_date"].split("/")
            #if (((today.month), today.day) < (Bday[1], Bday[2])):
            #    age = today.year - Bday[0] - 1
            #else:
            #    age = today.year - Bday[0]

            #Consulta para insertar usuario
            postgreSQL_insert_query = """INSERT INTO User_(user_name, user_lastname, user_password, user_email, user_telephone, user_photo,
                user_gender, user_born_date, user_address, user_age, user_rol_id, user_country_id, user_status_id)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""

            user_age = datetime.datetime.utcnow().year - datetime.datetime.strptime(data["birth_date"], "%Y-%m-%d").year
            record_to_insert = (
                data["name"],
                data["lastname"],
                data["password"],
                data["email"],
                data["phone"],
                data["photo"],
                data["gender"],
                data["birth_date"],
                data["address"],
                user_age,
                3,
                data["id_country"],
                1
            )
            cursor.execute(postgreSQL_insert_query, record_to_insert)
            
            #Consulta para obtener el id del usuario
            postgreSQL_getid_query = "SELECT user_id FROM User_ WHERE user_email = %s"
            cursor.execute(postgreSQL_getid_query, (data["email"],))
            user_id = cursor.fetchone()[0]

            #Envio de correo para validacion de usuario
            smtp = smtplib.SMTP('smtp.gmail.com', 587) 
            smtp.starttls() 
            smtp.login("softwareavanzado2022@gmail.com","Software12345!")
            link = "http://35.193.56.199:3005/esb/auth?id=" + str(user_id)
            message = """\
            Hola {str1}, muchas gracias por formar parte de la comunidad de SoccerStats. Es necesario que verifiques tu cuenta para poder acceder a todos los servicios.
            Pulsa en el siguiente enlace para verificar tu cuenta:\n\n\t\t\t\t {str2}""".format(str1 = data["name"], str2 = link)
            msg = MIMEText(message)
            msg["Subject"] = "Verificación de Cuenta (no-reply)"
            msg["From"] = "SoccerStats (no-reply)"
            msg["To"] = data["email"]
            smtp.sendmail("SoccerStats (no-reply)", data["email"], msg.as_string())
            smtp.quit()

            #Commit y cierre de conexion con base de datos
            connection.commit()
            cursor.close()
            connection.close()

            res["status"] = 200
            res["msg"] = "Usuario creado con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al guardar el usuario."
            res["data"].append("Usuario existente. (email)")
            
    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al guardar el usuario. (Excepción)"
        res["data"].append(str(error))
    finally:
        return jsonify(res)

#Verificar Usuario
@app.route('/esb/auth', methods = ['GET'])
def verifyUser():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar que exista un usuario con el id recibido
        user_id = request.args.get('id')
        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_id = %s"
        cursor.execute(find_user_query, (user_id,))

        if cursor.fetchone()[0] == 1:
            #Validar la cuenta del usuario
            verify_user_query = "UPDATE User_ SET user_validated = TRUE WHERE user_id = %s"
            cursor.execute(verify_user_query, (user_id,))
            connection.commit()
            res["status"] = 200
            res["msg"] = "Correo verificado con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al verificar correo."
            res["data"].append("Usuario inexistente. (id)")
            
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al verificar correo. (Excepción)"
        res["data"].append(str(error))
    finally:
        return jsonify(res)

#Login Usuario
@app.route('/esb/auth', methods = ['POST'])
def loginUser():
        res = { "status" : 0, "msg" : "", "data": []}
        data = request.get_json()
    #try:
        dbcreds = "user=admin_ge password=sa-ge host=18.189.78.76 port=5432 dbname=soccerstats"
        connection = psycopg2.connect(dbcreds)
        cursor = connection.cursor()

        #Consulta para obtener la contraseña del usuario
        get_password_query = """SELECT user_id, user_password, user_status_id, user_membership, user_rol_id,
            user_temporal_password, time_temporal_password FROM User_ WHERE user_email = %s"""
        cursor.execute(get_password_query, (data["email"],))
        user_data = cursor.fetchone()
        cursor.close()
        connection.close()

        #Verificar que exista el usuario
        if user_data:
            #Verificar si olvido contraseña
            if user_data[6] != None:

                #Verificar vencimiento de contraseña provisional
                time_now = datetime.datetime.now()
                dateTimeDifference = time_now - datetime.datetime.strptime(user_data[6], "%Y-%m-%d %H:%M:%S")
                dateTimeDifferenceInMin = dateTimeDifference.total_seconds() / 60
                if dateTimeDifferenceInMin >= 2: 

                    #Eliminacion de contraseña temporal
                    connection = psycopg2.connect(dbcreds)
                    cursor = connection.cursor()
                    update_password_query = "UPDATE User_ SET user_temporal_password = NULL, time_temporal_password = NULL WHERE user_email = %s"
                    cursor.execute(update_password_query, (data["email"],))
                    connection.commit()
                    cursor.close()
                    connection.close()
                    res["status"] = 400
                    res["msg"] = "Error de autenticación."
                    res["data"].append("Contraseña provisional vencida.")
                    return jsonify(res)

            #Obtener la contraseña a utilizar
            password = user_data[5] if user_data[5] != None else user_data[1]
            password_type = 1 if user_data[5] != None else 0

            #Verificar contraseña
            if password == data["password"]:
                #Generacion de token
                token = jwt.encode({
                    "id_user" : user_data[0],
                    "id_rol"  : user_data[4],
                    "exp" : datetime.datetime.utcnow() + datetime.timedelta(minutes = 1440)
                }, app.config["SECRET_KEY"], "HS256")

                res["status"] = 200
                res["msg"] = "Inicio de sesión exitoso."
                res["data"] = {
                    "token"             : token,
                    "id_status"         : user_data[2],
                    "id_rol"            : user_data[4],
                    "id_user"           : user_data[0],
                    "has_membership"    : user_data[3],
                    "password_type"     : password_type
                }
            else:
                res["status"] = 400
                res["msg"] = "Error de autenticación."
                res["data"].append("Contraseña invalida.")
        else:
            res["status"] = 400
            res["msg"] = "Error de autenticación."
            res["data"].append("Usuario inexistente. (email)")
        
    #except Exception as error:
    #    res["status"] = 400
    #    res["msg"] = "Error de autenticación. (Excepción)"
    #    res["data"].append(str(error))

    #finally:
        return jsonify(res)

#Envio y generacion de contraseña provisional
@app.route('/esb/user/temporal-password', methods = ['POST'])
def temporalPassword_():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        temporal_password =  str(uuid4())
        md5_password = hashlib.md5(temporal_password.encode("utf-8")).hexdigest()
        update_query = "UPDATE User_ SET user_temporal_password  = %s, time_temporal_password = %s WHERE user_email = %s"
        cursor.execute(update_query, (md5_password, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), data["email"]))
        
        #Envio de correo electronico con contraseña provisional
        smtp = smtplib.SMTP('smtp.gmail.com', 587)
        smtp.starttls() 
        smtp.login("softwareavanzado2022@gmail.com","Software12345!")
        link = "http://35.193.56.199:80/"
        message = """\
        Hola, a continuación encontrarás la contraseña provisional y el enlace para redirigirte al inicio de sesión\nContraseña Provisional: {str1}
        Pulsa en el siguiente enlace para ser redirigdo:\n\n\t\t\t\t {str2}""".format(str1 = temporal_password, str2 = link)
        msg = MIMEText(message)
        msg["Subject"] = "Contraseña Temporal (no-reply)"
        msg["From"] = "SoccerStats (no-reply)"
        msg["To"] = data["email"]
        smtp.sendmail("SoccerStats (no-reply)", data["email"], msg.as_string())
        smtp.quit()

        #Commit para permanencia de datos
        connection.commit()
        res["status"] = 200
        res["msg"] = "Se ha enviado un correo para restablecer la contraseña."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al enviar la contraseña temporal. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Restablecer Contraseña
@app.route('/esb/auth/reset-password', methods = ['POST'])
def temporalPassword():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener la contraseña del usuario
        get_password_query = """SELECT User_.user_id, User_.user_password
            FROM User_ WHERE User_.user_email = %s"""
        cursor.execute(get_password_query, (data["email"],))
        user_data = cursor.fetchone()
        cursor.close()
        connection.close()

        #Confirmacion de contraseña anterior para realizar el cambio
        if user_data[1] == data["temporal_password"]:
            update_query = "UPDATE User_ SET user_temporal_password  = %s WHERE user_email = %s"
            record_to_update = (
                    data["new_password"],
                    data["email"]                    
                )
            cursor.execute(update_query, record_to_update)
        
            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Se ha restablecido la contraseña"

            #Cierre de conexion con base de datos
            cursor.close()
            connection.close()
        else:
            res["status"] = 400
            res["msg"] = "Error al restablecer la contraseña."
            res["data"].append("Fallo la confirmación de contraseña anterior.")

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al restablecer la contraseña."
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Eliminar Usuario
@app.route('/user/delete', methods = ['DELETE'])
def deleteUser():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )

        cursor = connection.cursor()
        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_id = %s"
        cursor.execute(find_user_query, (data["id"],))

        if cursor.fetchone()[0] == 1:
            delete_user_query = "UPDATE User_ SET user_status_id = 3 WHERE user_id = %s"
            cursor.execute(delete_user_query, (data["id"],))
            connection.commit()
            res["status"] = 200
            res["msg"] = "Usuario eliminado con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al eliminar el usuario."
            res["data"].append("No existe ningún usuario con el id indicado.")

        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["message"] = "Error al eliminar el usuario. (Excepción)"
        res["data"].append(str(error))
    finally:
        return jsonify(res)

#Modificar Usuario
@app.route('/esb/customer/', methods = ['PUT'])
def updateUser():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_id = %s"
        cursor.execute(find_user_query, (data["id"],))

        if cursor.fetchone()[0] == 1:
            update_user_query = """UPDATE User_ SET user_name = %s, user_lastname = %s, user_password = %s, user_email = %s, user_telephone = %s,
                user_photo = %s, user_gender = %s, user_born_date = %s, user_address = %s, user_country_id = %s, user_age = %s
                WHERE user_id = %s"""
            user_age = datetime.datetime.utcnow().year - datetime.datetime.strptime(data["birth_date"], "%Y-%m-%d").year
            record_to_update = (
                data["name"],
                data["lastname"],
                data["password"],
                data["email"],
                data["phone"],
                data["photo"],
                data["gender"],
                data["birth_date"],   
                data["address"],
                data["id_country"],
                user_age,
                data["id"]
            )
            cursor.execute(update_user_query, record_to_update)

            
            res["status"] = 200
            res["msg"] = "Usuario actualizado con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar el usuario."
            res["data"].append("Usuario inexistente. (id)")

        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar el usuario. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Visualizar Perfil
@app.route('/esb/customer/', methods = ['GET'])
def visualizarPerfil():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar que exista un usuario con el id recibido
        user_id = request.args.get('id')
        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_id = %s"
        cursor.execute(find_user_query, (user_id,))

        if cursor.fetchone()[0] == 1:
            #Validar la cuenta del usuario
            verify_user_query = """SELECT u1.user_name, u1.user_lastname, u1.user_email, u1.user_telephone, u1.user_photo, u1.user_gender,
                    u1.user_born_date, u1.user_address, u1.user_age, c1.country_id, c1.country_name 
                FROM User_ AS u1
                    INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                WHERE u1.user_id = %s"""
            cursor.execute(verify_user_query, (user_id,))
            user_data = cursor.fetchone()

            for i in dropdownCountries:
                if(i['code'] == user_data[9]):
                    country = i['name']

            res["status"] = 200
            res["msg"] = "Usuario obtenido con éxito."
            res["data"].append({
                "name"          : user_data[0],
                "lastname"      : user_data[1],
                "email"         : user_data[2],
                "phone"         : user_data[3],
                "photo"         : user_data[4],
                "gender"        : user_data[5],
                "birth_date"    : user_data[6],
                "address"       : user_data[7],
                "age"           : user_data[8],
                "id_country"    : user_data[9],
                "country"       : user_data[10]
            })
            connection.commit()
                    
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener el usuario."
            res["data"].append("Usuario inexistente. (id)")
            
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener el usuario. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Comprar membresia
@app.route('/esb/customer/membership', methods=['POST'])
def comprarMembresia():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para actualizar membresia de usuario
        update_query = """UPDATE User_ SET user_membership = TRUE, membership_counter = membership_counter + 1, membership_money = membership_money + 174.99 WHERE user_id = %s"""
        query_values = (data["id_client"],)
        cursor.execute(update_query, query_values)

        #Commit para permanencia de datos y Cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()
        res["status"] = 200
        res["msg"] = "Membresía comprada con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al comprar membresía. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Canelar membresia
@app.route('/esb/customer/membership', methods=['PUT'])
def cancelarMembresia():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para actualizar membresia de usuario
        update_query = """UPDATE User_ SET user_membership = FALSE WHERE user_id = %s"""
        query_values = (data["id_client"],)
        cursor.execute(update_query, query_values)

        #Commit para permanencia de datos y Cierre de conexion con base de datos
        connection.commit()
        cursor.close()
        connection.close()
        res["status"] = 200
        res["msg"] = "Membresía cancelada con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al cancelar membresía. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Seguir Equipo
@app.route('/esb/customer/follow', methods=['POST'])
def followTeam():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que no se sigue al equipo
        verify_query = "SELECT COUNT(*) FROM Favorite WHERE id_user = %s AND id_team = %s"
        query_values = (data["id_client"], data["id_team"])
        cursor.execute(verify_query, query_values)

        if cursor.fetchone()[0] == 0:
            #Consulta para agregar a favoritos
            insert_query = "INSERT INTO Favorite(id_user, id_team) VALUES(%s, %s)"
            cursor.execute(insert_query, query_values)
            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Equipo agregado a favoritos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al agregar equipo a favoritos"
            res["data"].append("El usuario ya sigue a este equipo.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al agregar equipo a favoritos. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Obtener Equipos Seguidos
@app.route('/esb/customer/follow', methods=['GET'])
def getFollowedTeams():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        id_client = request.args.get('id_client')
        #Consulta para actualizar usuario
        get_followed_teams_query = """SELECT t1.team_id, t1.team_name, t1.team_foundation_date, t1.team_photo, c1.country_id, c1.country_name
            FROM User_ AS u1
                INNER JOIN Favorite AS f1 ON f1.id_user = u1.user_id
                INNER JOIN Team AS t1 ON t1.team_id = f1.id_team
                INNER JOIN Country AS c1 ON c1.country_id = t1.team_id_country
            WHERE u1.user_id = %s"""
        query_values = (id_client,)
        cursor.execute(get_followed_teams_query, query_values)

        #Retorno de equipos
        teams_data = cursor.fetchall()
        for team_data_ in teams_data:
            res["data"].append({
                "id"                : team_data_[0],
                "name"              : team_data_[1],
                "foundation_date"   : team_data_[2],
                "photo"             : team_data_[3],
                "id_country"        : team_data_[4],
                "country"           : team_data_[5]
            })

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()
        res["status"] = 200
        res["msg"] = "Favoritos obtenidos con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener los equipos favoritos. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Reporte1 - Jugadores o Técnico de X equipo
@app.route('/esb/customer/report/1', methods = ['GET'])
def report1():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si vienen los parametros
        player = request.args.get('player')
        id_team = request.args.get('id_team')
        if player != None or id_team != None:
            if player == False:
                #Consulta para retornar jugadores de X equipo
                user_query = """SELECT p1.player_id, p1.player_name, p1.player_lastname, c1.country_name, p1.player_photo, p1.player_position
                    FROM Player AS p1 INNER JOIN Country AS c1 ON c1.country_id = p1.player_id_nationality
                    WHERE p1.player_id_team = %s"""
                cursor.execute(user_query, (id_team,))
            else:
                #Consulta para retornar técnico de X equipo
                user_query = """SELECT p1.dt_id, p1.dt_name, p1.dt_lastname, c1.country_name, p1.dt_photo, ''
                    FROM DT AS p1 INNER JOIN Country AS c1 ON c1.country_id = p1.dt_id_country
                    WHERE p1.dt_id_team = %s"""
                cursor.execute(user_query, (id_team,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "nationality"   : user_data_[3],
                    "photo"         : user_data_[4],
                    "position"      : user_data_[5]
                })
            res["status"] = 200
            res["msg"] = "Jugadores o técnico del equipo obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener los jugadores o técnico del equipo."
            res["data"].append("Hace falta alguno de los parametros 'player' y/o 'id_team'.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener los jugadores o técnico del equipo. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Reporte2 - Jugadores o Técnicos mayores a X años
@app.route('/esb/customer/report/2', methods = ['GET'])
def report2():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si vienen los parametros
        age = request.args.get('age')
        player = request.args.get('player')
        if player != None or age != None:
            if player == False:
                #Consulta para retornar jugadores mayores a X años
                user_query = """SELECT p1.player_id, p1.player_name, p1.player_lastname, c1.country_name, p1.player_photo, p1.player_position, t1.team_name, p1.player_age
                    FROM Player AS p1
                        INNER JOIN Country AS c1 ON c1.country_id = p1.player_id_nationality
                        INNER JOIN Team AS t1 ON t1.team_id = p1.player_id_team
                    WHERE p1.player_age > %s"""
                cursor.execute(user_query, (age,))
            else:
                #Consulta para retornar técnico mayores a X años
                user_query = """SELECT p1.dt_id, p1.dt_name, p1.dt_lastname, c1.country_name, p1.dt_photo, '', t1.team_name, dt_age
                    FROM DT AS p1
                        INNER JOIN Country AS c1 ON c1.country_id = p1.dt_id_country
                        INNER JOIN Team AS t1 ON t1.team_id = p1.dt_id_team
                    WHERE p1.dt_age > %s"""
                cursor.execute(user_query, (age,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "nationality"   : user_data_[3],
                    "photo"         : user_data_[4],
                    "position"      : user_data_[5],
                    "team"          : user_data_[6],
                    "age"           : user_data_[7]
                })
            res["status"] = 200
            res["msg"] = "Jugadores o técnicos mayores a x años obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener los jugadores o técnicos mayores a x años."
            res["data"].append("Hace falta alguno de los parametros 'player' y/o 'age'.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener los jugadores o técnicos mayores a x años. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Reporte3 - Jugadores o Técnicos menores a X años
@app.route('/esb/customer/report/3', methods = ['GET'])
def report3():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si vienen los parametros
        age = request.args.get('age')
        player = request.args.get('player')
        if player != None or age != None:
            if player == False:
                #Consulta para retornar jugadores menores a X años
                user_query = """SELECT p1.player_id, p1.player_name, p1.player_lastname, c1.country_name, p1.player_photo, p1.player_position, t1.team_name, p1.player_age
                    FROM Player AS p1
                        INNER JOIN Country AS c1 ON c1.country_id = p1.player_id_nationality
                        INNER JOIN Team AS t1 ON t1.team_id = p1.player_id_team
                    WHERE p1.player_age < %s"""
                cursor.execute(user_query, (age,))
            else:
                #Consulta para retornar técnico menores a X años
                user_query = """SELECT p1.dt_id, p1.dt_name, p1.dt_lastname, c1.country_name, p1.dt_photo, '', t1.team_name, dt_age
                    FROM DT AS p1
                        INNER JOIN Country AS c1 ON c1.country_id = p1.dt_id_country
                        INNER JOIN Team AS t1 ON t1.team_id = p1.dt_id_team
                    WHERE p1.dt_age < %s"""
                cursor.execute(user_query, (age,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "nationality"   : user_data_[3],
                    "photo"         : user_data_[4],
                    "position"      : user_data_[5],
                    "team"          : user_data_[6],
                    "age"           : user_data_[7]
                })
            res["status"] = 200
            res["msg"] = "Jugadores o técnicos menores a x años obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener los jugadores o técnicos menores a x años."
            res["data"].append("Hace falta alguno de los parametros 'player' y/o 'age'.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener los jugadores o técnicos menores a x años. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)


#Reporte5 - 'Estadios con capacidad menor o igual a X'
@app.route('/esb/customer/report/8', methods = ['GET'])
def report5():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si vienen los parametros
        capacity = request.args.get('capacity')
        if capacity != None:
                #Consulta para retornar jugadores menores a X años
            user_query = """SELECT s.stadium_id, s.stadium_name, s.stadium_photo, s.stadium_id_country, s.stadium_capacity
                    FROM Stadium AS s
                    WHERE s.stadium_capacity <= %s"""
            cursor.execute(user_query, capacity)
            
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id_stadium"            : user_data_[0],
                    "stadium"          : user_data_[1],
                    "photo"      : user_data_[2],
                    "country"   : user_data_[3],
                    "capacity"         : user_data_[4]
                })
            res["msg"] = "Estadios con capacidad menor o igual a x obtenidos con éxito."
        cursor.close()
        connection.close()
    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener los estadios con capacidad menor o igual a x."
        res["data"].append(str(error))

    finally:
        return jsonify(res)


#Reporte6 - 'Partidos donde hubo al menos X goles'
@app.route('/esb/customer/report/11', methods = ['GET'])
def report6():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si vienen los parametros
        goals = request.args.get('goals')
        if goals != None:
                #Consulta para retornar jugadores menores a X años
            user_query = """SELECT m.match_id, m.match_game_date, m.match_attendees, m.match_result_local, m.match_result_visiting,m.match_status,
                    s.stadium_id, s.stadium_name, tl.team_id, tl.team_name, tl.team_photo, tv.team_id, tv.team_name, tv.team_photo, c.competition_id, c.competition_name
                    FROM Match_ AS m 
                    INNER JOIN Stadium AS s ON m.match_id_stadium = s.stadium_id
                    INNER JOIN Team AS tl ON m.match_id_team_local = tl.team_id
                    INNER JOIN Team AS tv ON m.match_id_team_visiting = tv.team_id
                    INNER JOIN Competition AS c on match_id_competition = c.competition_id
                    WHERE  (m.match_result_local + m.match_result_visiting) <=  %s"""
            cursor.execute(user_query, goals)
            
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id": user_data_[0],
                    "game_date": user_data_[1],
                    "attendees": user_data_[2],
                    "result_local": user_data_[3],
                    "result_visiting": user_data_[4],
                    "status": user_data_[5],
                    "id_stadium": user_data_[6],
                    "stadium": user_data_[7],
                    "id_team_local": user_data_[8],
                    "team_local": user_data_[9],
                    "photo_local": user_data_[10],
                    "id_team_visiting": user_data_[11],
                    "team_visiting": user_data_[12],
                    "photo_visiting": user_data_[13],
                    "id_competition": user_data_[14],
                    "competition": user_data_[15]
                })
            res["status"] = 200
            res["msg"] = "Partidos donde hubo x cantidad de goles obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener los partidos donde hubo x cantidad de goles."
            res["data"].append("Hace falta alguno de los parametros 'player' y/o 'age'.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()
    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener los estadios con capacidad menor o igual a x."
        res["data"].append(str(error))

    finally:
        return jsonify(res)



if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5000, debug = True)
