from flask import Flask, jsonify, request
from flask_socketio import SocketIO
from flask_cors import CORS
import psycopg2
import smtplib
from email.mime.text import MIMEText
from functools import wraps
import datetime


app = Flask(__name__)
CORS(app)
socket = SocketIO(app, cors_allowed_origins="*")


@app.route('/esb/administrator', methods = ['GET'])
def init():
    return "Microservicios admin trabajando correctamente."

#Crear usuario admin o empleado
@app.route('/esb/administrator/user', methods = ['POST'])
def createUser():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que no existe el usuario
        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_email = %s"
        cursor.execute(find_user_query, (data["email"],))
        if cursor.fetchone()[0] == 0:

            #Consulta para insertar usuario
            postgreSQL_insert_query = """INSERT INTO User_(user_name, user_lastname, user_password, user_email, user_telephone, user_photo,
                user_gender, user_born_date, user_address, user_age, user_validated, user_rol_id, user_country_id, user_status_id)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
            
            #Verificar si la edad se guarda o se obtiene al momento de consultarla
            user_age = datetime.datetime.utcnow().year - datetime.datetime.strptime(data["birth_date"], "%Y-%m-%d").year
            record_to_insert = (
                data["name"],
                data["lastname"],
                data["password"],
                data["email"],
                data["phone"],
                data["photo"],
                data["gender"],
                data["birth_date"],
                data["address"],
                user_age,
                True,
                data["id_rol"],
                data["id_country"],
                1
            )
            cursor.execute(postgreSQL_insert_query, record_to_insert)

            #Envio de correo para envio de credenciales del usuario
            smtp = smtplib.SMTP('smtp.gmail.com', 587) 
            smtp.starttls() 
            smtp.login("softwareavanzado2022@gmail.com","Software12345!")
            message = """\
            Hola {str1}, muchas gracias por formar parte de la comunidad de SoccerStats. Adjunto se encuentran las credenciales para que puedas acceder.
            Se recomienda cambiar la contraseña la primera vez que ingreses a la plataforma:\n\n\t\t\t\t {str2}""".format(str1 = data["name"], str2 = data["password"])
            msg = MIMEText(message)
            msg["Subject"] = "Credenciales Temporales (no-reply)"
            msg["From"] = "SoccerStats (no-reply)"
            msg["To"] = data["email"]
            smtp.sendmail("SoccerStats (no-reply)", data["email"], msg.as_string())
            smtp.quit()

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Usuario creado con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al guardar el usuario."
            res["data"].append("Ya existe una cuenta con el correo indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al guardar el usuario. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Cambio de status de un usuario
@app.route('/esb/administrator/user/status', methods = ['PUT'])
def updateStatusUser():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el usuario
        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_id = %s"
        cursor.execute(find_user_query, (data["id"],))
        if cursor.fetchone()[0] == 1:
            
            #Actualizar estado del usuario
            update_user_query = "UPDATE User_ SET user_status_id = %s WHERE user_id = %s"
            cursor.execute(update_user_query, (data["id_status"], data["id"]))

            #Insertar cambio en bitacora
            log_admin_query = "INSERT INTO Log_Admin(change_type, description_reason_change, admin_modificator_id, user_modificated_id) VALUES(%s,%s, %s, %s)"
            cursor.execute(log_admin_query, ("Cambio de status", data["description"], [0], data["id"]))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Estado del usuario actualizado con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar el estado del usuario."
            res["data"].append("Ya existe una cuenta con el correo indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar el estado del usuario. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Ver usuario/s
@app.route('/esb/administrator/user', methods = ['GET'])
def getUsers():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro id
        user_id = request.args.get('id')
        if user_id != None:
            #Consulta para obtener al usuario
            get_user_query = """SELECT user_name, user_lastname, user_email, user_telephone, user_photo, user_gender, user_born_date,
                user_address, user_age, user_rol_id, user_status_id, user_country_id FROM User_ WHERE user_id = %s"""
            cursor.execute(get_user_query, (user_id,))
            user_data = cursor.fetchone()
            if user_data:
                #Obtencion del nombre del pais
                get_country_query = "SELECT country_name FROM Country WHERE country_id = %s"
                cursor.execute(get_country_query, (user_data[11],))
                country_name = cursor.fetchone()[0]
                #Retorno de usuario
                res["status"] = 200
                res["msg"] = "Usuario(s) obtenido(s) con éxito."
                res["data"].append({
                    "id"        : user_id,
                    "name"      : user_data[0],
                    "lastname"  : user_data[1],
                    "email"     : user_data[2],
                    "phone"     : user_data[3],
                    "photo"     : user_data[4],
                    "gender"    : user_data[5],
                    "birth_date": user_data[6],
                    "address"   : user_data[7],
                    "age"       : user_data[8],
                    "id_rol"    : user_data[9],
                    "id_status" : user_data[10],
                    "id_country": user_data[11],
                    "country"   : country_name
                })
            else:
                res["status"] = 400
                res["msg"] = "Error al obtener usuario(s)."
                res["data"].append("No existe ningún usuario con el id indicado.")
        else:
            #Consulta para obtener todos los usuarios junto con los datos de su pais
            get_users_query = """SELECT
                u1.user_id, u1.user_name, u1.user_lastname, u1.user_email, u1.user_telephone, u1.user_photo, u1.user_gender,
                u1.user_born_date, u1.user_address, u1.user_age, u1.user_rol_id, u1.user_status_id, u1.user_country_id, c1.country_name
                FROM User_ AS u1 INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id"""
            cursor.execute(get_users_query)

            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"        : user_data_[0],
                    "name"      : user_data_[1],
                    "lastname"  : user_data_[2],
                    "email"     : user_data_[3],
                    "phone"     : user_data_[4],
                    "photo"     : user_data_[5],
                    "gender"    : user_data_[6],
                    "birth_date": user_data_[7],
                    "address"   : user_data_[8],
                    "age"       : user_data_[9],
                    "id_rol"    : user_data_[10],
                    "id_status" : user_data_[11],
                    "id_country": user_data_[12],
                    "country"   : user_data_[13]
                })
            res["status"] = 200
            res["msg"] = "Usuario(s) obtenido(s) con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()
        
    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuario(s). (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Modificar usuario
@app.route('/esb/administrator/user', methods = ['PUT'])
def updateUser():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para verificar que exista el usuario
        find_user_query = "SELECT COUNT(*) FROM User_ WHERE user_id = %s"
        cursor.execute(find_user_query, (data["id"],))
        if cursor.fetchone()[0] == 1:
            
            #Actualizacion de datos del usuario
            update_user_query = """UPDATE User_ SET user_name = %s, user_lastname = %s, user_password = %s, user_email = %s, user_telephone = %s,
            user_photo = %s, user_gender = %s, user_born_date = %s, user_address = %s, user_age = %s, user_country_id = %s WHERE user_id = %s"""
            user_age = datetime.datetime.utcnow().year - datetime.datetime.strptime(data["birth_date"], "%Y-%m-%d").year
            record_to_update = (
                data["name"],
                data["lastname"],
                data["password"],
                data["email"],
                data["phone"],         
                data["photo"],
                data["gender"],        
                data["birth_date"],  
                data["address"],
                user_age,     
                data["id_country"],
                data["id"]
            )
            cursor.execute(update_user_query, record_to_update)

            #Insertar cambio en bitacora
            log_admin_query = "INSERT INTO Log_Admin(change_type, description_reason_change, admin_modificator_id, user_modificated_id) VALUES(%s,%s, %s, %s)"
            cursor.execute(log_admin_query, ("Actualización de usuario", data["description"], [0], data["id"]))

            #Commit para permanencia de datos
            connection.commit()
            res["status"] = 200
            res["msg"] = "Usuario actualizado con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al actualizar el usuario."
            res["data"].append("No existe ningún usuario con el id indicado.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al actualizar el usuario. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Usuarios suscrito a X equipo
@app.route('/esb/administrator/report/1', methods = ['GET'])
def report1():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro membership
        id_team = request.args.get('id_team')
        if id_team != None:

            #Consulta para retornar usuarios suscrito a X equipo
            favorites_query = """SELECT DISTINCT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo
                FROM User_ AS u1
                    INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                    INNER JOIN Favorite AS f1 ON f1.id_user = u1.user_id
                WHERE f1.id_team = %s"""
            cursor.execute(favorites_query, (id_team,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "nationality"   : user_data_[3],
                    "photo"         : user_data_[4]
                })
            res["status"] = 200
            res["msg"] = "Usuarios suscritos al equipo x obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener usuarios suscritos al equipo x."
            res["data"].append("Parametro 'membership' faltante.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuarios suscritos al equipo x. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Usuarios con o sin membresia
@app.route('/esb/administrator/report/2', methods = ['GET'])
def report2():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro membership
        membership = request.args.get('membership')
        if membership != None:

            #Consulta para retornar usuarios con o sin membresia
            membership_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo
                FROM User_ AS u1 INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                WHERE u1.user_membership = %s"""
            cursor.execute(membership_query, (membership,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "nationality"   : user_data_[3],
                    "photo"         : user_data_[4]
                })
            res["status"] = 200
            res["msg"] = "Usuarios con o sin membresía obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener usuarios con o sin membresía."
            res["data"].append("Parametro 'membership' faltante.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuarios con o sin membresía. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Usuarios que Mas membresías han adquirido
@app.route('/esb/administrator/report/3', methods = ['GET'])
def report3():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para retornar usuarios que Mas membresías han adquirido
        favorites_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo
            FROM User_ AS u1
                INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
            ORDER BY u1.membership_counter DESC LIMIT 10"""
        cursor.execute(favorites_query)
        
        #Retorno de usuarios
        users_data = cursor.fetchall()
        for user_data_ in users_data:
            res["data"].append({
                "id"            : user_data_[0],
                "name"          : user_data_[1],
                "lastname"      : user_data_[2],
                "nationality"   : user_data_[3],
                "photo"         : user_data_[4]
            })
        res["status"] = 200
        res["msg"] = "Usuarios con mas membresías obtenidos con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuarios con mas membresías. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Usuarios que más dinero han gastado
@app.route('/esb/administrator/report/4', methods = ['GET'])
def report4():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para retornar usuarios que más dinero han gastado
        favorites_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo
            FROM User_ AS u1
                INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
            ORDER BY u1.membership_money DESC LIMIT 10"""
        cursor.execute(favorites_query)
        
        #Retorno de usuarios
        users_data = cursor.fetchall()
        for user_data_ in users_data:
            res["data"].append({
                "id"            : user_data_[0],
                "name"          : user_data_[1],
                "lastname"      : user_data_[2],
                "nationality"   : user_data_[3],
                "photo"         : user_data_[4]
            })
        res["status"] = 200
        res["msg"] = "Usuarios con mas dinero gastado obtenidos con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuarios con mas dinero gastado. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Usuarios de x pais
@app.route('/esb/administrator/report/5', methods = ['GET'])
def report5():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro id_country
        id_country = request.args.get('id_country')
        if id_country != None:

            #Consulta para retornar usuarios de x pais
            country_users_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, u1.user_photo
                FROM User_ AS u1 INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                WHERE c1.country_id = %s"""
            cursor.execute(country_users_query, (id_country,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "photo"         : user_data_[3]
                })
            res["status"] = 200
            res["msg"] = "Usuarios de x país obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener usuarios de x país."
            res["data"].append("Parametro 'id_country' faltante.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuarios de x país. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Usuarios de x genero
@app.route('/esb/administrator/report/6', methods = ['GET'])
def report6():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro gender
        gender = request.args.get('gender')
        if gender != None:

            #Consulta para retornar usuarios de x genero
            gender_users_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, u1.user_photo, c1.country_name
                FROM User_ AS u1 INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                WHERE u1.user_gender = %s"""
            cursor.execute(gender_users_query, (gender,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "photo"         : user_data_[3],
                    "nationality"   : user_data_[4]
                })
            res["status"] = 200
            res["msg"] = "Usuarios de x genero obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener usuarios de x genero."
            res["data"].append("Parametro 'gender' faltante.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuarios de x genero. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Usuarios con al menos x años de edad
@app.route('/esb/administrator/report/7', methods = ['GET'])
def report7():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro age
        age = request.args.get('age')
        if age != None:

            #Consulta para retornar usuarios con al menos x años de edad
            age_users_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, u1.user_photo, c1.country_name, u1.user_born_date, u1.user_age
                FROM User_ AS u1 INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                WHERE u1.user_age >= %s"""
            cursor.execute(age_users_query, (age,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "photo"         : user_data_[3],
                    "nationality"   : user_data_[4],
                    "birth_date"    : user_data_[5],
                    "age"           : user_data_[6]
                })
            res["status"] = 200
            res["msg"] = "Usuarios con al menos x años de edad, obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener usuarios con al menos x años de edad."
            res["data"].append("Parametro 'age' faltante.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener usuarios con al menos x años de edad. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Empleados que mas/menos noticias han publicado
@app.route('/esb/administrator/report/8', methods = ['GET'])
def report8():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro order
        order = request.args.get('order')
        if order != None:
            if order == 0:
                #Consulta para retornar empleados que mas noticias han publicado
                notice_employees_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo, COUNT(*) AS count_
                    FROM User_ AS u1
                        INNER JOIN Notice_ AS n1 ON n1.notice_id_user = u1.user_id
                        INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                    GROUP BY u1.user_id, c1.country_id ORDER BY count_ DESC LIMIT 5"""
                cursor.execute(notice_employees_query)
            else:
                #Consulta para retornar empleados que menos noticias han publicado
                notice_employees_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo, COUNT(*) AS count_
                    FROM User_ AS u1
                        INNER JOIN Notice_ AS n1 ON n1.notice_id_user = u1.user_id
                        INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                    GROUP BY u1.user_id, c1.country_id ORDER BY count_ ASC LIMIT 5"""
                cursor.execute(notice_employees_query)
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "nationality"   : user_data_[3],
                    "photo"         : user_data_[4],
                    "count"         : user_data_[5]
                })
            res["status"] = 200
            res["msg"] = "Empleados con mas o menos noticias publicadas, obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener empleados con mas o menos noticias publicadas."
            res["data"].append("Parametro 'order' faltante.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener empleados con mas o menos noticias publicadas. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Empleados que mas/menos noticias han publicado de x equipo
@app.route('/esb/administrator/report/9', methods = ['GET'])
def report9():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Verificar si viene el parametro order
        order = request.args.get('order')
        id_team = request.args.get('id_team')
        if order != None or id_team != None:
            if order == 0:
                #Consulta para retornar empleados que mas noticias han publicado de x equipo
                notice_employees_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo, COUNT(*) AS count_
                    FROM User_ AS u1
                        INNER JOIN Notice_ AS n1 ON n1.notice_id_user = u1.user_id
                        INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                    WHERE n1.notice_id_team = %s
                    GROUP BY u1.user_id, c1.country_id ORDER BY count_ DESC LIMIT 5"""
                cursor.execute(notice_employees_query, (id_team,))
            else:
                #Consulta para retornar empleados que menos noticias han publicado de x equipo
                notice_employees_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, c1.country_name, u1.user_photo, COUNT(*) AS count_
                    FROM User_ AS u1
                        INNER JOIN Notice_ AS n1 ON n1.notice_id_user = u1.user_id
                        INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                    WHERE n1.notice_id_team = %s
                    GROUP BY u1.user_id, c1.country_id ORDER BY count_ ASC LIMIT 5"""
                cursor.execute(notice_employees_query, (id_team,))
            
            #Retorno de usuarios
            users_data = cursor.fetchall()
            for user_data_ in users_data:
                res["data"].append({
                    "id"            : user_data_[0],
                    "name"          : user_data_[1],
                    "lastname"      : user_data_[2],
                    "nationality"   : user_data_[3],
                    "photo"         : user_data_[4],
                    "count"         : user_data_[5]
                })
            res["status"] = 200
            res["msg"] = "Empleados con mas o menos noticias publicadas de x equipo, obtenidos con éxito."
        else:
            res["status"] = 400
            res["msg"] = "Error al obtener empleados con mas o menos noticias publicadas de x equipo."
            res["data"].append("Hace falta alguno de los parametros 'order' y/o 'id_team'.")

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener empleados con mas o menos noticias publicadas de x equipo. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

#Bitacora de administradores
@app.route('/esb/administrator/report/10', methods = ['GET'])
def report10():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para retornar bitacora de los administradores
        age_users_query = """SELECT u1.user_id, u1.user_name, u1.user_lastname, u1.user_photo, r1.rol_description,
            c1.change_type, c1.log_date, c1.description_reason_change, c1.admin_modificator_id
            FROM User_ AS u1
            INNER JOIN Log_Admin AS c1 ON c1.user_modificated_id = u1.user_id
            INNER JOIN Rol AS r1 ON r1.rol_id = u1.user_rol_id"""
        cursor.execute(age_users_query)
        
        #Retorno de usuario y bitacora
        users_data = cursor.fetchall()
        for user_data_ in users_data:
            res["data"].append({
                "id"            : user_data_[0],
                "user_name"     : user_data_[1],
                "user_lastname" : user_data_[2],
                "user_photo"    : user_data_[3],
                "user_role"     : user_data_[4],
                "action"        : user_data_[5],
                "date"          : user_data_[6],
                "description"   : user_data_[7],
                "admin_id"      : user_data_[8],
            })
        res["status"] = 200
        res["msg"] = "Bitácora de los administradores obtenida con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener bitácora de los administradores. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 5011, debug = True)
