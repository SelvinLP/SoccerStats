CREATE TABLE IF NOT EXISTS Rol(
	rol_id 			SERIAL PRIMARY KEY,
	rol_name 		CHAR NOT NULL,
	rol_description TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS Country(
	country_id 		SERIAL PRIMARY KEY,
	country_name 	TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS User_Status(
	user_status_id 				SERIAL PRIMARY KEY,
	user_status_name 			TEXT NOT NULL,
	user_status_description		TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS User_(
	user_id 			SERIAL PRIMARY KEY,
	user_name 			TEXT NOT NULL,
	user_lastname 		TEXT NOT NULL,
	user_password 		TEXT NOT NULL,
	user_email 			TEXT UNIQUE NOT NULL,	
	user_telephone 		TEXT NOT NULL,
	user_photo 			TEXT NOT NULL,
	user_gender 		TEXT NOT NULL,
	user_born_date 		DATE NOT NULL,
	user_created_date 	DATE NOT NULL DEFAULT CURRENT_DATE,
	user_address 		TEXT NOT NULL,
	user_membership 	BOOLEAN NOT NULL DEFAULT FALSE,
	user_age 			INTEGER NOT NULL,
	user_validated		BOOLEAN NOT NULL DEFAULT FALSE,
	user_rol_id 		INTEGER NOT NULL,
	user_country_id 	INTEGER NOT NULL,
	user_status_id 		INTEGER NOT NULL,
	CONSTRAINT user_fk1 FOREIGN KEY (user_rol_id) REFERENCES Rol (rol_id),
	CONSTRAINT user_fk2 FOREIGN KEY (user_country_id) REFERENCES Country (country_id),
	CONSTRAINT user_fk3 FOREIGN KEY (user_status_id) REFERENCES User_Status (user_status_id)
);
ALTER TABLE User_ ADD user_temporal_password TEXT NULL;
ALTER TABLE User_ ADD time_temporal_password DATE NULL;
ALTER TABLE User_ ADD membership_counter INTEGER NOT NULL DEFAULT 0;
ALTER TABLE User_ ADD membership_money NUMERIC NOT NULL DEFAULT 0.0;

CREATE TABLE IF NOT EXISTS Log_Admin(
	log_admin_id 				SERIAL PRIMARY KEY,
	log_date					DATE NOT NULL DEFAULT CURRENT_DATE,
	change_type					TEXT NOT NULL,
	description_reason_change	TEXT NOT NULL,
	admin_modificator_id 		INTEGER NOT NULL,
	user_modificated_id			INTEGER NOT NULL,
	CONSTRAINT log_admin_fk1 FOREIGN KEY (admin_modificator_id) REFERENCES User_ (user_id),
	CONSTRAINT log_admin_fk2 FOREIGN KEY (user_modificated_id) REFERENCES User_ (user_id)
)

CREATE TABLE IF NOT EXISTS Stadium(
	stadium_id				SERIAL PRIMARY KEY,
	stadium_name			TEXT NOT NULL,
	stadium_foundation_date DATE NOT NULL,
	stadium_capacity		INTEGER NOT NULL,
	stadium_address			TEXT NOT NULL,
	stadium_photo			TEXT NOT NULL,
	stadium_status			INTEGER NOT NULL,
	stadium_id_country		INTEGER NOT NULL,
	CONSTRAINT stadium_fk1 FOREIGN KEY(stadium_id_country) REFERENCES Country(country_id)
);

CREATE TABLE IF NOT EXISTS Team(
	team_id 				SERIAL PRIMARY KEY,
	team_name 				TEXT NOT NULL,
	team_foundation_date 	DATE NOT NULL,
	team_photo				TEXT NOT NULL,
	team_id_country 		INTEGER NOT NULL,
	CONSTRAINT team_fk1 FOREIGN KEY(team_id_country) REFERENCES Country(country_id)
);

CREATE TABLE IF NOT EXISTS DT(
	dt_id			SERIAL PRIMARY KEY,
	dt_name			TEXT NOT NULL,
	dt_lastname		TEXT NOT NULL,
	dt_birth_date	DATE NOT NULL,
	dt_photo		TEXT NOT NULL,
	dt_status		INTEGER NOT NULL,
	dt_id_team		INTEGER NOT NULL,
	dt_id_country	INTEGER NOT NULL,
	CONSTRAINT dt_fk1 FOREIGN KEY(dt_id_team) REFERENCES Team(team_id),
	CONSTRAINT dt_fk2 FOREIGN KEY(dt_id_country) REFERENCES Country(country_id)
);
ALTER TABLE DT ADD dt_age INTEGER NOT NULL DEFAULT 0;

CREATE TABLE IF NOT EXISTS Player(
	player_id				SERIAL PRIMARY KEY,
	player_name				TEXT NOT NULL,
	player_lastname			TEXT NOT NULL,
	player_birth_date		DATE NOT NULL,
	player_id_nationality	INTEGER NOT NULL,
	player_position			INTEGER NOT NULL,
	player_status			INTEGER NOT NULL,
	player_photo			TEXT NOT NULL,
	player_id_team			INTEGER NOT NULL,
	CONSTRAINT player_fk1 FOREIGN KEY(player_id_team) REFERENCES Team(team_id),
	CONSTRAINT player_fk2 FOREIGN KEY(player_id_nationality) REFERENCES Country(country_id)
);
ALTER TABLE Player ADD player_age INTEGER NOT NULL DEFAULT 0;

CREATE TABLE IF NOT EXISTS Competition(
	competition_id			SERIAL PRIMARY KEY,
	competition_name		TEXT NOT NULL,
	competition_type		INTEGER NOT NULL,
	competition_year		INTEGER NOT NULL,	
	competition_champion_id	INTEGER NOT NULL,
	competition_country_id	INTEGER NOT NULL,
	CONSTRAINT competition_fk1 FOREIGN KEY(competition_champion_id) REFERENCES Team(team_id),
	CONSTRAINT competition_fk2 FOREIGN KEY(competition_country_id) REFERENCES Country(country_id)
);

CREATE TABLE IF NOT EXISTS Match_(
	match_id				SERIAL PRIMARY KEY,
	match_game_date			DATE NOT NULL,
	match_attendees			INTEGER NOT NULL,
	match_result_local		INTEGER NOT NULL,
	match_result_visiting	INTEGER NOT NULL,
	match_status			INTEGER NOT NULL,
	match_id_stadium		INTEGER NOT NULL,
	match_id_team_local		INTEGER NOT NULL,
	match_id_team_visiting	INTEGER NOT NULL,
	match_id_competition	INTEGER NOT NULL,
	CONSTRAINT match_fk1 FOREIGN KEY(match_id_stadium) REFERENCES Stadium(stadium_id),
	CONSTRAINT match_fk2 FOREIGN KEY(match_id_team_local) REFERENCES Team(team_id),
	CONSTRAINT match_fk3 FOREIGN KEY(match_id_team_visiting) REFERENCES Team(team_id),
	CONSTRAINT match_fk4 FOREIGN KEY(match_id_competition) REFERENCES Competition(competition_id)
);

CREATE TABLE IF NOT EXISTS Log_Player(
	log_id 			SERIAL PRIMARY KEY,
	log_date		DATE NOT NULL DEFAULT CURRENT_DATE,
	start_date		DATE NOT NULL,
	end_date 		DATE NOT NULL,
	log_team_id		INTEGER NOT NULL,
	log_player_id	INTEGER NOT NULL,
	CONSTRAINT log_player_fk1 FOREIGN KEY (log_team_id) REFERENCES Team (team_id),
	CONSTRAINT log_player_fk2 FOREIGN KEY (log_player_id) REFERENCES Player (player_id)
)

CREATE TABLE IF NOT EXISTS Log_DT(
	log_id 			SERIAL PRIMARY KEY,
	log_date		DATE NOT NULL DEFAULT CURRENT_DATE,
	start_date		DATE NOT NULL,
	end_date 		DATE NOT NULL,
	log_team_id		INTEGER NOT NULL,
	log_dt_id		INTEGER NOT NULL,
	CONSTRAINT log_dt_fk1 FOREIGN KEY (log_team_id) REFERENCES Team (team_id),
	CONSTRAINT log_dt_fk2 FOREIGN KEY (log_dt_id) REFERENCES DT (dt_id)
);

CREATE TABLE IF NOT EXISTS Incidence(
	incidence_id			SERIAL PRIMARY KEY,
	incidence_type			INTEGER NOT NULL,
	incidence_description	TEXT NOT NULL,
	incidence_minute		TEXT NOT NULL,
	incidence_id_player		INTEGER NOT NULL,
	incidence_id_game		INTEGER NOT NULL,
	CONSTRAINT incidence_fk1 FOREIGN KEY(incidence_id_player) REFERENCES Player(player_id),
	CONSTRAINT incidence_fk2 FOREIGN KEY(incidence_id_game) REFERENCES Match_(match_id)
);

CREATE TABLE IF NOT EXISTS Notice_(
	notice_id			SERIAL PRIMARY KEY,
	notice_title		TEXT NOT NULL,
	notice_description	TEXT NOT NULL,
	notice_date			DATE NOT NULL,
	notice_id_team		INTEGER NOT NULL,
	notice_id_user		INTEGER NOT NULL,
	CONSTRAINT notice_fk1 FOREIGN KEY(notice_id_team) REFERENCES Team(team_id),
	CONSTRAINT notice_fk2 FOREIGN KEY(notice_id_user) REFERENCES User_(user_id)
);

CREATE TABLE IF NOT EXISTS Favorite(
	id_favorite		SERIAL PRIMARY KEY,
	id_user			INTEGER NOT NULL,
	id_team			INTEGER NOT NULL,
	follow_date		DATE NOT NULL DEFAULT CURRENT_DATE,
	CONSTRAINT favorite_fk1 FOREIGN KEY(id_user) REFERENCES User_(user_id),
	CONSTRAINT favorite_fk2 FOREIGN KEY(id_team) REFERENCES	Team(team_id)
);

CREATE TABLE IF NOT EXISTS Esb(
	id_esb 	SERIAL PRIMARY KEY,
	ip 		TEXT NOT NULL,
	portu 	TEXT NOT NULL,
	porta	TEXT NOT NULL,
	porte	TEXT NOT NULL
);

SELECT * FROM Competition;
SELECT user_password, user_temporal_password, time_temporal_password FROM User_ WHERE user_email = 'javierbran73@gmail.com';
SELECT CURRENT_DATE;
DROP TABLE Team;
INSERT INTO Esb(ip, portu, porta, porte) VALUES ('35.223.13.4', '5000', '5001', '5002');
INSERT INTO Rol(rol_name, rol_description) VALUES ('A', 'administrador'),('E', 'empleado'),('C', 'cliente');
INSERT INTO Country(country_name) VALUES ('guatemala');
INSERT INTO User_Status(user_status_name, user_status_description) VALUES ('A', 'activo'), ('C', 'congelado'), ('E', 'eliminado');
INSERT INTO User_(user_name, user_lastname, user_password, user_email, user_telephone, user_photo,
				 user_gender, user_born_date, user_created_date, user_address, user_membership,
				 user_age, user_rol_id, user_country_id, user_status_id)
VALUES ('javier', 'bran', '1234', 'javierbran73@gmail.com', 12345678, 'photo.jpg', 
	   'M', '1997/12/22', '2022/03/01', 'zona1, guatemala', TRUE, 24, 1, 88, 1);
INSERT INTO Team(team_name, team_foundation_date, team_photo, team_id_country)
	VALUES ('Sin equipo', CURRENT_DATE, 'photo.png', 1);

DELETE FROM user_ WHERE user_email = '3030921730108@ingenieria.usac.edu.gt';

SELECT p1.dt_id, p1.dt_name, p1.dt_lastname, c1.country_name, p1.dt_photo, 0
FROM DT AS p1 INNER JOIN Country AS c1 ON c1.country_id = p1.dt_id_country
WHERE p1.dt_id_team = 3;

SELECT u1.user_id, u1.user_name, u1.user_lastname, u1.user_photo
                FROM User_ AS u1 INNER JOIN Country AS c1 ON c1.country_id = u1.user_country_id
                WHERE c1.country_id = %s
				
SELECT * FROM Country;
SELECT *
	FROM Team AS t1 INNER JOIN Country AS c1 ON c1.country_id = t1.team_id_country
		WHERE c1.country_id = 88;







