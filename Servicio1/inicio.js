'use strict';

const express = require('express');
const bodyParser = require('body-parser');

// Constants
const PORT = '8085'; //process.env.PUERTO_SERVICIO1;
const HOST = '0.0.0.0'; //process.env.HOST_SERVICIO1;
const MENSAJE = process.env.MENSAJE;
const IP_BASEDATOS = process.env.IP_BASEDATOS;

// App
const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.get('/', (req, res) => {
  res.send('Software avanzado!!! Grupo E ');
});

app.listen(PORT, HOST);
console.log(`Running on http://localhot:8085`);
