from flask import Flask, jsonify, request
from flask_socketio import SocketIO
from flask_cors import CORS
from functools import wraps
import requests
import psycopg2
import jwt


app = Flask(__name__)
CORS(app)
socket = SocketIO(app, cors_allowed_origins="*")
app.config['SECRET_KEY'] = 'SiSaleSA_'


#Autenticacion JWT
def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'authorization' in request.headers:
            token = request.headers['authorization']
            token = token.split(' ')[1]
        if not token:
            return jsonify({'status': -1, 'message': 'Token valido faltante.'})
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms = ["HS256"])

            #Conexion con base de datos PostgreSQL
            connection = psycopg2.connect(
                user        = "admin_ge",
                password    = "sa-ge",
                host        = "18.189.78.76",
                port        = "5432",
                database    = "soccerstats"
            )
            cursor = connection.cursor()

            #Consulta para obtener los datos del usuario
            postgreSQL_get_user_query = "SELECT * FROM User_ WHERE User_.user_id = %s"
            cursor.execute(postgreSQL_get_user_query, (data["id_user"],))
            current_user = cursor.fetchone()
            
        except Exception as error:
            return jsonify({'status': -1, 'message': 'Token invalido.', 'error': str(error)})
        return f(current_user, *args, **kwargs)
    return decorator


@app.route('/', methods = ['GET'])
@token_required
def init():
    return "Middleware working correctly."


#------------------------ CONFIGURACION IP Y PUERTOS ---------------------------#


@app.route('/esb', methods = ['POST'])
def setIpPorts():
    res = { "status" : 0, "msg" : "", "data": []}
    data = request.get_json()
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para actualizar ip y puertos
        update_query = "UPDATE Esb SET ip = %s, portu = %s, porta = %s, porte = %s WHERE id_esb = 1"
        cursor.execute(update_query, (data["ip"], data["portu"], data["porta"], data["porte"]))

        #Commit para permanencia de datos
        connection.commit()
        res["status"] = 200
        res["msg"] = "ESB configurado con éxito."

        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al configurar ESB. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)

@app.route('/esb', methods = ['GET'])
def getIpPorts():
    res = { "status" : 0, "msg" : "", "data": []}
    try:
        #Conexion con base de datos PostgreSQL
        connection = psycopg2.connect(
            user        = "admin_ge",
            password    = "sa-ge",
            host        = "18.189.78.76",
            port        = "5432",
            database    = "soccerstats"
        )
        cursor = connection.cursor()

        #Consulta para obtener ip y puertos
        get_match_query = "SELECT ip, portu, porta, porte FROM Esb WHERE id_esb = 1"
        cursor.execute(get_match_query)
        res["data"].append(cursor.fetchone())
        
        #Cierre de conexion con base de datos
        cursor.close()
        connection.close()

        res["status"] = 200
        res["msg"] = "ESB obtenido con éxito."

    except Exception as error:
        res["status"] = 400
        res["msg"] = "Error al obtener ESB. (Excepción)"
        res["data"].append(str(error))

    finally:
        return jsonify(res)


#------------------------- CLIENTE Y AUTENTICACION ----------------------------#


@app.route('/esb/customer/register', methods = ['POST'])
def createUser():
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[1] + '/esb/customer/register', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/auth', methods = ['GET'])
def verifyUser():
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            user_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/auth'
            endpoint = endpoint + '?id=' + user_id if user_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/auth', methods = ['POST'])
def loginUser():
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[1] + '/esb/auth', json = input)
            output = req.json() 
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/auth/temporal-password', methods = ['POST'])
def temporalPassword():
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[1] + '/esb/user/temporal-password', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer', methods = ['DELETE'])
@token_required
def deleteUser(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.delete('http://' + data[0] + ":" + data[1] + '/user/delete', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/', methods = ['PUT'])
@token_required
def updateUser(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[1] + '/esb/customer/', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/', methods = ['GET'])
@token_required
def visualizarPerfil(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            user_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/customer/'
            endpoint = endpoint + '?id=' + user_id if user_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/membership', methods=['POST'])
@token_required
def comprarMembresia(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[1] + '/esb/customer/membership', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/membership', methods=['PUT'])
@token_required
def cancelarMembresia(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[1] + '/esb/customer/membership', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/follow', methods=['POST'])
@token_required
def followTeam(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[1] + '/esb/customer/follow', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/follow', methods=['GET'])
@token_required
def getFollowedTeams(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            user_id = request.args.get('id_client')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/customer/follow'
            endpoint = endpoint + '?id_client=' + user_id if user_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/report/1', methods = ['GET'])
@token_required
def report1_c(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            id_team = request.args.get('id_team')
            player = request.args.get('player')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/costumer/report/1'
            endpoint = endpoint + '?id_team=' + id_team + '&player=' + player if id_team != None and player != None else endpoint
            req = requests.get(endpoint)
            output = req.json()    
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/report/2', methods = ['GET'])
@token_required
def report2_c(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            age = request.args.get('age')
            player = request.args.get('player')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/costumer/report/2'
            endpoint = endpoint + '?age=' + age + '&player=' + player if age != None and player != None else endpoint
            req = requests.get(endpoint)
            output = req.json()    
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/report/3', methods = ['GET'])
@token_required
def report3_c(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            age = request.args.get('age')
            player = request.args.get('player')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/costumer/report/3'
            endpoint = endpoint + '?age=' + age + '&player=' + player if age != None and player != None else endpoint
            req = requests.get(endpoint)
            output = req.json()    
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/report/8', methods = ['GET'])
@token_required
def report5_c(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            capacity = request.args.get('capacity')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/customer/report/8'
            endpoint = endpoint + '?capacity=' + capacity if capacity != None else endpoint
            req = requests.get(endpoint)
            output = req.json()    
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/customer/report/11', methods = ['GET'])
@token_required
def report6_c(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            goals = request.args.get('goals')
            endpoint = 'http://' + data[0] + ":" + data[1] + '/esb/customer/report/11'
            endpoint = endpoint + '?goals=' + goals if goals != None else endpoint
            req = requests.get(endpoint)
            output = req.json()    
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

#------------------------- ADMIN ----------------------------#


@app.route('/esb/administrator/user', methods = ['POST'])
@token_required
def createUser_(current_user):
    #try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[2] + '/esb/administrator/user', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    #except Exception as e:
    #    return e

@app.route('/esb/administrator/user/status', methods = ['PUT'])
@token_required
def updateStatusUser(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[2] + '/esb/administrator/user/status', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/user', methods = ['GET'])
@token_required
def getUsers(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            user_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/user'
            endpoint = endpoint + '?id=' + user_id if user_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e:
        return e

@app.route('/esb/administrator/user', methods = ['PUT'])
@token_required
def updateUser_(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[2] + '/esb/administrator/user', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/1', methods = ['GET'])
@token_required
def report1_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            id_team = request.args.get('id_team')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/1'
            endpoint = endpoint + '?id_team=' + id_team if id_team != None else endpoint
            req = requests.get(endpoint)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/2', methods = ['GET'])
@token_required
def report2_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            membership = request.args.get('membership')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/2'
            endpoint = endpoint + '?membership=' + membership if membership != None else endpoint
            req = requests.get(endpoint)
            output = req.json()   
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/3', methods = ['GET'])
@token_required
def report3_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/3'
            req = requests.get(endpoint)
            output = req.json()   
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/4', methods = ['GET'])
@token_required
def report4_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/4'
            req = requests.get(endpoint)
            output = req.json()   
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/5', methods = ['GET'])
@token_required
def report5_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            id_country = request.args.get('id_country')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/5'
            endpoint = endpoint + '?id_country=' + id_country if id_country != None else endpoint
            req = requests.get(endpoint)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/6', methods = ['GET'])
@token_required
def report6_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            gender = request.args.get('gender')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/6'
            endpoint = endpoint + '?gender=' + gender if gender != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/7', methods = ['GET'])
@token_required
def report7_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            age = request.args.get('age')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/7'
            endpoint = endpoint + '?age=' + age if age != None else endpoint
            req = requests.get(endpoint)
            output = req.json()       
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/8', methods = ['GET'])
@token_required
def report8_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            order = request.args.get('order')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/8'
            endpoint = endpoint + '?order=' + order if order != None else endpoint
            req = requests.get(endpoint)
            output = req.json()    
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/9', methods = ['GET'])
@token_required
def report9_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            order = request.args.get('order')
            id_team = request.args.get('id_team')
            endpoint = 'http://' + data[0] + ":" + data[2] + '/esb/administrator/report/9'
            endpoint = endpoint + '?order=' + order + '&id_team=' + id_team if order != None and id_team != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/administrator/report/10', methods = ['GET'])
@token_required
def report10_a(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            req = requests.get('http://' + data[0] + ":" + data[2] + '/esb/administrator/report/10')
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e


#------------------------- EMPLOYEE ----------------------------#


@app.route('/esb/team', methods = ['POST'])
@token_required
def createTeam(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/team', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/team', methods = ['GET'])
@token_required
def getTeams(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            team_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[3] + '/esb/team'
            endpoint = endpoint + '?id=' + team_id if team_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/team', methods = ['PUT'])
@token_required
def updateTeam(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[3] + '/esb/team', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/team', methods = ['DELETE'])
@token_required
def deleteTeam(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.delete('http://' + data[0] + ":" + data[3] + '/esb/team', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/stadium', methods = ['POST'])
@token_required
def createStadium(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/stadium', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/stadium', methods = ['GET'])
@token_required
def getStaduims(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            stadium_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[3] + '/esb/stadium'
            endpoint = endpoint + '?id=' + stadium_id if stadium_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/stadium', methods = ['PUT'])
@token_required
def updateStadium(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[3] + '/esb/stadium', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/stadium', methods = ['DELETE'])
@token_required
def deleteStadium(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.delete('http://' + data[0] + ":" + data[3] + '/esb/stadium', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/technical-director', methods = ['POST'])
@token_required
def createDT(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/technical-director', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/technical-director', methods = ['GET'])
@token_required
def getDTs(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            dt_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[3] + '/esb/technical-director'
            endpoint = endpoint + '?id=' + dt_id if dt_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()     
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/technical-director', methods = ['PUT'])
@token_required
def updateDT(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[3] + '/esb/technical-director', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/technical-director', methods = ['DELETE'])
@token_required
def deleteDT(current_user):
    try:    
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.delete('http://' + data[0] + ":" + data[3] + '/esb/technical-director', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/player', methods = ['POST'])
@token_required
def createPlayer(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/player', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/player', methods = ['GET'])
@token_required
def getPlayers(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0] 
            player_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[3] + '/esb/player'
            endpoint = endpoint + '?id=' + player_id if player_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/player', methods = ['PUT'])
@token_required
def updatePlayer(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0] 
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[3] + '/esb/player', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/player', methods = ['DELETE'])
@token_required
def deletePlayer(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0] 
            input = request.get_json()
            req = requests.delete('http://' + data[0] + ":" + data[3] + '/esb/player', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/competition', methods = ['POST'])
@token_required
def createCompetition(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0] 
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/competition', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/competition', methods = ['GET'])
@token_required
def getCompetitions(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0] 
            competition_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[3] + '/esb/competition'
            endpoint = endpoint + '?id=' + competition_id if competition_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/competition', methods = ['PUT'])
@token_required
def updateCompetition(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[3] + '/esb/competition', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/competition', methods = ['DELETE'])
@token_required
def deleteCompetition(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.delete('http://' + data[0] + ":" + data[3] + '/esb/competition', json =  input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/match', methods = ['POST'])
@token_required
def createMatch(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/match', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/match', methods = ['GET'])
@token_required
def getMatches(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            match_id = request.args.get('id')
            endpoint = 'http://' + data[0] + ":" + data[3] + '/esb/match'
            endpoint = endpoint + '?id=' + match_id if match_id != None else endpoint
            req = requests.get(endpoint)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/match', methods = ['PUT'])
@token_required
def updateMatch(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.put('http://' + data[0] + ":" + data[3] + '/esb/match', json = input)
            output = req.json()
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/match', methods = ['DELETE'])
@token_required
def deleteMatch(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.delete('http://' + data[0] + ":" + data[3] + '/esb/match', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/employee/player-transfer', methods = ['POST'])
@token_required
def transferPlayer(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/employee/player-transfer', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/employee/technical-director-transfer', methods = ['POST'])
@token_required
def transferDT(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/employee/technical-director-transfer', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/employee/incidence', methods = ['POST'])
@token_required
def createIncidence(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/employee/incidence', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

@app.route('/esb/post', methods = ['POST'])
@token_required
def createPost(current_user):
    try:
        esb_res = requests.get('http://localhost:3005/esb')
        esb_output = esb_res.json()
        if esb_output["status"] == 200:
            data = esb_output["data"][0]
            input = request.get_json()
            req = requests.post('http://' + data[0] + ":" + data[3] + '/esb/post', json = input)
            output = req.json()        
            return output
        else:
            return esb_output
    except Exception as e: 
        return e

    
if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 3005, debug = True)